<?php
include_once 'web_builder.php';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| 
| Aquí es donde se registran las rutas web para la aplicación. Estas
| las rutas son cargadas por el RouteServiceProvider dentro de un grupo que
| contiene el grupo de middleware "web". 
|
*/

Route::pattern('slug', '[a-z0-9- _]+');

Route::group(['prefix' => 'admin', 'namespace'=>'Admin'], function () {

  # páginas de error que se deben mostrar sin necesidad de iniciar sesión
    Route::get('404', function () {
        return view('admin/404');
    });

    Route::get('500', function () {
        return view('admin/500');
    });
    # Lock screen
    Route::get('{id}/lockscreen', 'UsersController@lockscreen')->name('lockscreen');
    Route::post('{id}/lockscreen', 'UsersController@postLockscreen')->name('lockscreen');
    # RUTAS BÁSICAS
    Route::get('login', 'AuthController@getSignin')->name('login');
    Route::get('signin', 'AuthController@getSignin')->name('signin');
    Route::post('signin', 'AuthController@postSignin')->name('postSignin');
   // Route::post('signup', 'AuthController@postSignup')->name('admin.signup');
    Route::post('forgot-password', 'AuthController@postForgotPassword')->name('forgot-password');
    # Forgot Password Confirmation
    Route::get('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm')->name('forgot-password-confirm');
    Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm');

    # Logout
    Route::get('logout', 'AuthController@getLogout')->name('logout');

    # Account Activation
    Route::get('activate/{userId}/{activationCode}', 'AuthController@getActivate')->name('activate');
});



// RUTAS ESPECIALES PARA EL ADMINISTRADOR



Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {
    # GUI Crud Generator
    Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('generator_builder');
    Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');
    Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');
    // Model checking
    Route::post('modelCheck', 'ModelcheckController@modelCheck');

    # Dashboard / Index
    Route::get('/', 'JoshController@showHome')->name('dashboard');
    # crop demo
    Route::post('crop_demo', 'JoshController@crop_demo')->name('crop_demo');
    //Log viewer routes
  

 
    # Activity log
    Route::get('activity_log/data', 'JoshController@activityLogData')->name('activity_log.data');
//    Route::get('/', 'JoshController@index')->name('index');
});









Route::group(['prefix' => 'admin','namespace'=>'Admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {

    # GESTIÓN DE USUARIOS
    Route::group([ 'prefix' => 'users'], function () {
        Route::get('data', 'UsersController@data')->name('users.data');
        Route::get('{user}/delete', 'UsersController@destroy')->name('users.delete');
        Route::get('{user}/confirm-delete', 'UsersController@getModalDelete')->name('users.confirm-delete');
        Route::get('{user}/restore', 'UsersController@getRestore')->name('restore.user');
//        Route::post('{user}/passwordreset', 'UsersController@passwordreset')->name('passwordreset');
        Route::post('passwordreset', 'UsersController@passwordreset')->name('passwordreset');

    });
    Route::resource('users', 'UsersController');

    Route::get('deleted_users',['before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'])->name('deleted_users');

    # GESTIÓN DE GRUPOS O ROLES
        Route::group(['prefix' => 'groups'], function () {
        Route::get('{group}/delete', 'GroupsController@destroy')->name('groups.delete');
        Route::get('{group}/confirm-delete', 'GroupsController@getModalDelete')->name('groups.confirm-delete');
        Route::get('{group}/restore', 'GroupsController@getRestore')->name('groups.restore');
    });
    Route::resource('groups', 'GroupsController');

   

   
  
    /*routes for file*/
    Route::group(['prefix' => 'file'], function () {
        Route::post('create', 'FileController@store')->name('store');
        Route::post('createmulti', 'FileController@postFilesCreate')->name('postFilesCreate');
        Route::delete('delete', 'FileController@delete')->name('delete');
    });

    Route::get('crop_demo', function () {
        return redirect('admin/imagecropping');
    });



 

 

});


# UPLOADS O CARGA DE MULTIMEDIA

    Route::group(['prefix' => 'uploads', 'middleware' => 'user'], function () {

  
    Route::get('{id}/confirm-delete', 'UploadController@getModalDelete')->name('uploads.confirm-delete');    
    Route::resource('uploads','UploadController');
    Route::get('uploads/{id}/delete', 'UploadController@getDelete')->name('uploads.delete');  


});

    # GALERIA

    Route::group(['prefix' => 'galeria', 'middleware' => 'user'], function () {

  
    Route::get('{id}/confirm-delete', 'GaleriaController@getModalDelete')->name('galeria.confirm-delete');    
    Route::resource('galeria','GaleriaController');
    Route::get('galeria/{id}/delete', 'GaleriaController@getDelete')->name('galeria.delete');  


});

    # MENSAJES DE USUARIOS

    Route::group(['prefix' => 'mensajeUsuarios', 'middleware' => 'user'], function () {

  
    Route::get('{id}/confirm-delete', 'MensajeUsuariosController@getModalDelete')->name('mensajeUsuarios.confirm-delete');    
    Route::resource('mensajeUsuarios','MensajeUsuariosController');
    Route::get('mensajeUsuarios/{id}/delete', 'MensajeUsuariosController@getDelete')->name('mensajeUsuarios.delete');  


});

      # EMPRESAS

    Route::group(['prefix' => 'empresas', 'middleware' => 'user'], function () {

  
    Route::get('{id}/confirm-delete', 'EmpresaController@getModalDelete')->name('empresas.confirm-delete');    
    Route::resource('empresas','EmpresaController');
    Route::get('empresas/{id}/delete', 'EmpresaController@getDelete')->name('empresas.delete');  


});

  # PROYECTOS

    Route::group(['prefix' => 'proyectos', 'middleware' => 'user'], function () {

  
    Route::get('{id}/confirm-delete', 'ProyectoController@getModalDelete')->name('proyectos.confirm-delete');    
    Route::resource('proyectos','ProyectoController');
    Route::get('proyectos/{id}/delete', 'ProyectoController@getDelete')->name('proyectos.delete');  


});

      # RESIDUOS

    Route::group(['prefix' => 'residuos', 'middleware' => 'user'], function () {

  
    Route::get('{id}/confirm-delete', 'ResiduoController@getModalDelete')->name('residuos.confirm-delete');    
    Route::resource('residuos','ResiduoController');
    Route::get('residuos/{id}/delete', 'ResiduoController@getDelete')->name('residuos.delete');  


});


   # RECOLECCIÓN DE RESIDUOS

    Route::group(['prefix' => 'recoleccionResiduos', 'middleware' => 'user'], function () {

  
    Route::get('{id}/confirm-delete', 'Recoleccion_residuoController@getModalDelete')->name('recoleccionResiduos.confirm-delete');    
    Route::resource('recoleccionResiduos','Recoleccion_residuoController');
    Route::get('recoleccionResiduos/{id}/delete', 'Recoleccion_residuoController@getDelete')->name('recoleccionResiduos.delete');  


});


# ESTADISTICAS DE RECOLECCIÓN DE RESIDUOS



     Route::group(['prefix' => 'reportes', 'middleware' => 'cliente'], function () {

        
         Route::get('estadisticas', 'EstadisticasResiduosController@indexestadisticas')->name('estadisticasresiduos');
         Route::get('certificado', 'EstadisticasResiduosController@certificadoIndex')->name('certificado');
         Route::get('reporteexcel', 'EstadisticasResiduosController@reporteexcelIndex')->name('reporteexcel');
         Route::post('certificado', 'EstadisticasResiduosController@certificadopdf')->name('certificadopdf');  
         Route::post('reporteexcel', 'EstadisticasResiduosController@reporteexcel')->name('descargarexcel');       
         Route::get('graficobarras', 'EstadisticasResiduosController@indexestadisticas')->name('graficobarras_get');
         Route::post('graficobarras', 'EstadisticasResiduosController@estadisticasresiduos')->name('graficobarras');

   });






# Las páginas restantes se llamarán desde el método del controlador inferior
# en el escenario del mundo real, es posible que deba definir todas las rutas manualmente

#PAGINAS GENERADAS POR EL CRUD
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('{name?}', 'JoshController@showView');
});





#FrontEndController
Route::get('login', 'FrontEndController@getLogin')->name('login');
Route::post('login', 'FrontEndController@postLogin')->name('login');
//Route::get('register', 'FrontEndController@getRegister')->name('register');
//Route::post('register','FrontEndController@postRegister')->name('register');
//Route::get('activate/{userId}/{activationCode}','FrontEndController@getActivate')->name('activate');
Route::get('forgot-password','FrontEndController@getForgotPassword')->name('forgot-password');
Route::post('forgot-password', 'FrontEndController@postForgotPassword');

# Confirmación al recuperar contraseña 
Route::post('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');
Route::get('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@getForgotPasswordConfirm')->name('forgot-password-confirm');







# Mi cuenta mostrar y actualizar información
Route::group(['middleware' => 'user'], function () {
    Route::put('my-account', 'FrontEndController@update');
    Route::get('my-account', 'FrontEndController@myAccount')->name('my-account');
});
Route::get('logout', 'FrontEndController@getLogout')->name('logout');
# contact form

//Route::post('contactenos_send', 'EmailController@postContact')->name('contact');






# RUTAS INDEX 

Route::get('/', 'IndexController@index')->name('home'); 



Route::get('buildingsite','RedireccionController@index')->name('buildingsite_1');
Route::get('contactenos', 'EmailController@index')->name('contactenos');
Route::post('contactenos_send', 'EmailController@postContact')->name('contactenos_send');

