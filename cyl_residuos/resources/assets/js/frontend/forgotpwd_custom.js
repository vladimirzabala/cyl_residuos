$(".omb_loginForm").bootstrapValidator({
    fields: {
        email: {
            validators: {
                notEmpty: {
                    message: 'The email address is required'
                },
                emailAddress: {
                    message: 'Dirección de correo electrónico inválida'
                }
            }
        }
    }
});
$(function() {
    setTimeout(function () {
        $("#notific").remove();
    }, 5000);
});