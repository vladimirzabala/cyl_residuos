<table class="table table-responsive" id="uploads-table" width="100%">
    <thead>
     <tr>
         <th>Consecutivo</th>
                  <th>Creado en</th>
        <th>Nombre Sistema</th>
         <th>URL</th>
        <th>Descripcion</th>
        <th>Tipo</th>
         <th>Sitio</th>
        <th><center>Vista previa</center></th>
        <th >Acción</th>
     </tr>
    </thead>
    <tbody>
    @foreach($uploads as $upload)
        <tr>
             <td>{!! $upload->id!!}</td>
               <td>{!! $upload->created_at!!}</td>

            <td>{!! $upload->filename !!}</td>
             <td>{!! '/uploads/files/'.$upload->filename !!}</td>
            <td>{!! $upload->descripcion !!}</td>
            <td>{!! $upload->tipo !!}</td>
               <td>{!! $upload->sitio !!}</td>
             <td><center><a href="{{asset('/uploads/files/'.$upload->filename)}}" target="_blank">
            <i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="editar contenido"></i>
            Visualizar</a></center></td>

            <td>
                 <a href="{{ route('uploads.show', $upload->id) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view upload"></i>
                 </a>
              <!--   <a href="{{ route('uploads.edit', $upload->id) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit upload"></i>
                 </a> -->
                 <a href="{{ route('uploads.confirm-delete', $upload->id) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete upload"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
 <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#uploads-table').DataTable({
                      responsive: true,
                      pageLength: 20
                  });
                  $('#uploads-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop