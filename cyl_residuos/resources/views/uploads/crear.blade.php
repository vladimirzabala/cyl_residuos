@extends('layouts.usuarios')

@section('title')
Crear Multimedia
@parent
@stop
 <style type="text/css">
            .btn-file {
                position: relative;
                overflow: hidden;
            }
            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }
        </style>

@section('content')

<section class="content-header">
    <h1>Subir multimedia</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Inicio
            </a>
        </li>
        <li>Uploads</li>
        <li class="active">Crear multimedia </li>
    </ol>
</section>
@include('core-templates::common.errors')
  @include('flash::message')
<section class="content paddingleft_right15">
<div class="row">
 <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Subir archivo
            </h4></div>
        <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'uploads.store', 'method' => 'post', 'files' => true]) !!}

            @include('uploads.fields')

        {!! Form::close() !!}


    </div>
    <br>

  </div>

 </div>

</section>
 @stop
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("form").submit(function() {
                $('input[type=submit]').attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@stop
