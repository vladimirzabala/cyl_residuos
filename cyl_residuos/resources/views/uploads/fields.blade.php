<!-- Filename Field -->
<div class="form-group col-sm-12">
 
                      
</div>
<div class="clearfix"></div>

<!-- Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control','placeholder' => 'Ingrese el nombre del archivo']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('lbltipo', 'Tipo:') !!}
    {!! Form::select('tipo', ['PDF' => 'PDF', 'IMAGEN' => 'IMAGEN', 'WORD' => 'WORD', 'EXCEL' => 'EXCEL','ZIP' => 'ZIP','RAR' => 'RAR','MP3'=>'MP3','POWER POINT PPTX' => 'ARCHIVO POWER POINT PPTX'], null, ['class' => 'form-control','placeholder' => 'Por favor seleccione el tipo de archivo']) !!}
</div>





<div class="form-group col-sm-6">
   {!! Form::label('filename', 'Archivo:') !!}&nbsp;&nbsp;&nbsp; 
                       
<span class="btn btn-default btn-file">
   Seleccione un archivo {!! Form::file('file') !!}
</span>

<br>
<br>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('uploads.index') !!}" class="btn btn-default">Cancelar</a>
</div>



