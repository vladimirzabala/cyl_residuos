<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $upload->id !!}</p>
    <hr>
</div>

<!-- Filename Field -->
<div class="form-group">
    {!! Form::label('filename', 'Filename:') !!}
    <p>{!! $upload->filename !!}</p>
    <hr>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('Tipo', 'Tipo:') !!}
    <p>{!! $upload->tipo !!}</p>
    <hr>
</div>

