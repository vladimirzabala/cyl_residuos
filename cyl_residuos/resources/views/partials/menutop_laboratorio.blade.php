<div class="wrapper_sup" style="position: fixed;width: 100%;z-index: 1000;top: 0;text-shadow: 0 0 black;background:#fff;">
  
        <div class="menuleft">
            <a href="{{ route('home') }}">
                <img src="{{ asset('assets/images/Megaproyecto_Lia2.png') }}" alt="" style="height:130px;width:140px">
            </a>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>



<div class="menuright">
    <a href="javascript:void(0);" class="ico_menu link_menu"></a>
    <nav class="menu_top f15">

                    <ul class="" style="text-shadow:none;">              
                  
               
                         @foreach ($menus as $key => $item)
                             
                                   <li class="dropdown">
                                       <a href="{{$item->destino}}" class="dropbtn">{{$item->nombre}}</a>    


                                                   <div class="dropdown-content">
                                                         @foreach ($submenus as $key => $subitem)

                                                                     @if ($subitem->menupadre  == $item->id )
                                                                    
                                                                                    <a href="{{$subitem->destino}}">{{$subitem->nombre}}</a>             
                                                                     
                                                                    @endif 

                                                          @endforeach

                                                   </div> 

                                 </li>

                          @endforeach
            

              @if(Sentinel::guest())
                         <li><a href="{{ URL::to('login') }}" >   <i class="livicon" data-name="sign-in" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="Iniciar sesión"></i></a>
                        </li>                        
                @else
                    <li class="dropdown">
                             <a href="#" class="dropbtn"> Mi cuenta </a>           
                             <div class="dropdown-content">
                                    <a href="{{ URL::to('my-account') }}">Mi perfil</a>
                                    <a href="{{ URL::to('logout') }}">Cerrar sesión</a>
                              </div>
                                
                      </li>



                    @endif
                        </ul>
                   
                

            </nav>
      </div>

</div> 



            
   