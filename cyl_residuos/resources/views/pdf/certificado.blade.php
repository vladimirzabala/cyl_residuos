<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Certificado</title>
     {!! Html::style('assets/css/pdf.css') !!}
 <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine"> -->
 <link href="https://fonts.googleapis.com/css?family=Ruda&display=swap" rel="stylesheet">
   
     <style>
     body
     {
     /* font-family: 'Tangerine', serif;
        font-size: 14px;*/
        font-family: 'Ruda', sans-serif;
        font-size: 14px
     }
     </style>
     </head>
  <body>


<img src="{{ asset('assets/images/background_certificado_2019.jpg') }}" alt="" width="810px" height="1125px" style=" position:absolute;z-index: 1;"  />

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>



<div class="container" style="text-align: justify;">

<p class="titulo">CERTIFICADO DE DISPOSICIÓN FINAL Y/O APROVECHAMIENTO</p>

<p class="titulo">C&L SOLUCIONES EMPRESARIALES S.A.S. NIT 900.982.822-5</p>


<p><center>Con registro ante la SECRETARÍA DISTRITAL DE AMBIENTE Nº 2019EE38005 de 2019 como Recuperador Específico</center></p>
<p class="no_certificado"><b>CERTIFICADO: No. {!! $no_certificado !!} de {!! $fecha_certificado!!}</b></p>
<br>
<p class="titulo">CERTIFICA</p>

<span class="contenido">
Que transportó para aprovechamiento y valorización los siguientes materiales excedentes del proceso constructivo, dando cumplimiento a lo establecido en el Decreto 586 de 2015 y Resolución 01115 de 2012, por parte de la empresa





@foreach($informacion_proyecto as $proyecto) 
{!! $proyecto->nombre_empresa !!}.</span>
<br>
<br>
<span class="datos_recoleccion">PUNTO DE RECOLECCIÓN:</span>
   {!! $proyecto->puntorecoleccion !!}<br>
<span class="datos_recoleccion">CIUDAD:</span>
{!! $proyecto->ciudad !!}<br>
<span class="datos_recoleccion">GENERADOR:</span>
 {!! $proyecto->generador !!}<br>
 <span class="datos_recoleccion">DIRECCIÓN:</span>
 {!! $proyecto->direccion !!}
 <br>

@endforeach





<br>

<table class="table table-responsive"  width="100%">
    <thead class="cabezera_tabla">
     <tr>
        <th>TIPO DE RESIDUO</th>
         <th><center>FECHA RETIRO</center></th>
        <th>CANTIDAD</th>
        <th>UNIDAD</th>
        <th>TRATAMIENTO</th>
     </tr>
    </thead>
    <tbody>

     @foreach($residuos as $residuo)
        <tr>
            <td>{!! $residuo->nombre !!}</td>
                 <td>{!! $residuo->fecha !!}</td>
            <td><center>{!! $residuo->cantidad !!}</center></td>
            <td><center>{!! $residuo->unidad !!}</center></td>
            <td>
                {!! $residuo->tratamiento !!}
            </td>
        </tr>
            @endforeach

    </tbody>
</table>
<span>Fecha de expedición: {!! $fecha_expedicion !!}</span>
<br><br><br><br><br>

<div>




<span>_____________________________________</span><br>
<span>MICHAEL ALEXANDER ARBOLEDA L.</span><br>
<span>REPRESENTANTE LEGAL</span>
<br>
<br>
<img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate($codigo)) }} ">

</div>




    
  </body>
</html>