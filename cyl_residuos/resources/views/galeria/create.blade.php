@extends('layouts/usuarios')

@section('title')
Galeria
@parent
@stop

@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>Galeria</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Inicio
            </a>
        </li>
        <li>Galerias</li>
        <li class="active">Create Galeria </li>
    </ol>
</section>
<section class="content paddingleft_right15">
<div class="row">
 <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> <i class="livicon" data-name="image" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Agregar imagen a la galeria
            </h4></div>
        <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'galeria.store']) !!}

            @include('galeria.fields')

        {!! Form::close() !!}
    </div>
  </div>
 </div>
</section>
 @stop
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("form").submit(function() {
                $('input[type=submit]').attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@stop
