@extends('layouts/usuarios')

@section('title')
Galeria
@parent
@stop

@section('content')
<section class="content-header">
    <h1>Galeria View</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Inicio
            </a>
        </li>
        <li>Galerias</li>
        <li class="active">Ver Galeria</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
       <div class="panel panel-primary">
        <div class="panel-heading clearfix">
            <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Galeria details
            </h4>
        </div>
            <div class="panel-body">
                @include('galeria.show_fields')
            </div>
        </div>
    <div class="form-group">
           <a href="{!! route('galeria.index') !!}" class="btn btn-default">Back</a>
    </div>
  </div>
</section>
@stop
