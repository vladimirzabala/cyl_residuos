<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $galeria->id !!}</p>
    <hr>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $galeria->nombre !!}</p>
    <hr>
</div>

<!-- Ruta Field -->
<div class="form-group">
    {!! Form::label('ruta', 'Ruta:') !!}
    <p>{!! $galeria->ruta !!}</p>
    <hr>
</div>

<!-- Peso Field -->
<div class="form-group">
    {!! Form::label('peso', 'Peso:') !!}
    <p>{!! $galeria->peso !!}</p>
    <hr>
</div>

