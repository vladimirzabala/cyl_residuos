@extends('layouts/usuarios')

@section('title')
Galerias
@parent
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Galeria</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
             Inicio
            </a>
        </li>
        <li>Galerias</li>
        <li class="active">Imagenes en galeria</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="image" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Imagenes en galeria
                </h4>
                <div class="pull-right">
                    <a href="{{ route('galeria.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> Crear</a>
                </div>
            </div>
            <br />
            <div class="panel-body table-responsive">
                 @include('galeria.table')
                 
            </div>
        </div>
 </div>
</section>
@stop
