@extends('layouts/default')

{{-- Page title --}}
@section('title')
Escríbanos 
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/contact.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}


{{-- Page content --}}
@section('content')
    <!-- Map Section Start -->
   
    <!-- //map Section End -->
   
   
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
    
    <!-- Container Section Start -->
    <div class="container">
        <div class="row">
            <!-- Contact form Section Start -->
            <div class="col-md-12">
                <h2>Escríbanos un mensaje</h2>
                <!-- Notifications -->
                <div id="notific">
                @include('notifications')
                </div>
                <form class="contact" id="contact" action="{{route('contactenos_send')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <input type="text" name="contact-name" class="form-control input-lg" placeholder="Nombres y apellidos" required>
                    </div>

                    <div class="form-group">
                        <input type="email" name="contact-email" class="form-control input-lg" placeholder="Correo electrónico" email>
                    </div>
                      <div class="form-group">
                        <input type="text" name="contact-name" class="form-control input-lg" placeholder="Asunto" required>
                    </div>
                    <div class="form-group">
                        <textarea name="contact-msg" class="form-control input-lg no-resize resize_vertical" rows="6" placeholder="Mensaje" required></textarea>
                    </div>
                    <div class="input-group">
                        <button class="btn btn-primary" type="submit">Enviar</button>
                        <button class="btn btn-default" type="reset">Limpiar</button>
                    </div>
                </form>
            </div>
            <!-- //Conatc Form Section End -->
            <!-- Address Section Start -->
           
            <!-- //Address Section End -->
        </div>
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->

  
    <!--page level js ends-->
 

@stop
