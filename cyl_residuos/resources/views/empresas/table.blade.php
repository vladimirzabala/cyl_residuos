<table class="table table-responsive" id="empresas-table" width="100%">
    <thead>
     <tr>
     <th>ID</th>
        <th>Nombre</th>
        <th>Nit</th>
        <th>Tipo</th>
        <th>Direccion</th>
        <th>Telefono</th>
        <th >Acción</th>
     </tr>
    </thead>
    <tbody>
    @foreach($empresas as $empresa)
        <tr>
         <td>{!! $empresa->id !!}</td>
            <td>{!! $empresa->nombre !!}</td>
            <td>{!! $empresa->nit !!}</td>
            <td>{!! $empresa->tipo !!}</td>
            <td>{!! $empresa->direccion !!}</td>
            <td>{!! $empresa->telefono !!}</td>
            <td>
                 <a href="{{ route('empresas.show', $empresa->id) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view empresa"></i>
                 </a>
                 <a href="{{ route('empresas.edit', $empresa->id) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit empresa"></i>
                 </a>
                 <a href="{{ route('empresas.confirm-delete', $empresa->id) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete empresa"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
 <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#empresas-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#empresas-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop