<!-- Nombre Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12">
    {!! Form::label('idempresa', 'Idempresa:') !!}
     {!! Form::number('idempresa', null, ['class' => 'form-control','placeholder'=>'Ingrese el id de la emrpesa ej. 11 (solo se aceptan números)']) !!}
   
</div>
<div class="form-group col-sm-12">
    {!! Form::label('puntorecoleccion', 'Punto recolección:') !!}
    {!! Form::text('puntorecoleccion', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('ciudad', 'Ciudad:') !!}
    {!! Form::text('ciudad', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('direccion', 'Dirección:') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!} 
</div>

<div class="form-group col-sm-12">
    {!! Form::label('generador', 'Generador:') !!}
    {!! Form::text('generador', null, ['class' => 'form-control']) !!}
</div>


<!-- Descripcion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Idempresa Field -->


<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('proyectos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
