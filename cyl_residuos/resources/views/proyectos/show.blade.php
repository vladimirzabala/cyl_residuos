@extends('layouts/usuarios')

@section('title')
Proyecto
@parent
@stop

@section('content')
<section class="content-header">
    <h1>Ver proyecto</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Proyectos</li>
        <li class="active">Ver proyecto</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
       <div class="panel panel-primary">
        <div class="panel-heading clearfix">
            <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Detalles de proyecto
            </h4>
        </div>
            <div class="panel-body">
                @include('proyectos.show_fields')
            </div>
        </div>
    <div class="form-group">
           <a href="{!! route('proyectos.index') !!}" class="btn btn-default">Volver</a>
    </div>
  </div>
</section>
@stop
