<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id (consecutivo dentro de la base de datos):') !!}
    <p>{!! $proyecto->id !!}</p>
    <hr>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $proyecto->nombre !!}</p>
    <hr>
</div>

<div class="form-group">
    {!! Form::label('idempresa', 'Idempresa:') !!}
    <p>{!! $proyecto->idempresa !!}</p>
    <hr>
</div>
<div class="form-group">
    {!! Form::label('puntorecoleccion', 'Puntorecoleccion:') !!}
    <p>{!! $proyecto->puntorecoleccion !!}</p>
    <hr>
</div>

<div class="form-group">
    {!! Form::label('ciudad', 'Ciudad:') !!}
    <p>{!! $proyecto->ciudad !!}</p>
    <hr>
</div>

<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $proyecto->direccion !!}</p>
    <hr>
</div>

<div class="form-group">
    {!! Form::label('generador', 'Generador:') !!}
    <p>{!! $proyecto->generador !!}</p>
    <hr>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $proyecto->descripcion !!}</p>
    <hr>
</div>

<!-- Idempresa Field -->


