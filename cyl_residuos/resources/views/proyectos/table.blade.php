<table class="table table-responsive" id="proyectos-table" width="100%">
    <thead>
     <tr>
      <th>No.proyecto</th>
        <th>Nombre</th>
        <th>Punto de recolección</th>
        <th>Ciudad</th>       
        <th>Generador</th>
         <th>Idempresa</th>
        <th >Acción</th>
     </tr>
    </thead>
    <tbody>
    @foreach($proyectos as $proyecto)
        <tr>
        <td>{!! $proyecto->id !!}</td>
            <td>{!! $proyecto->nombre !!}</td>
            <td>{!! $proyecto->puntorecoleccion !!}</td>
             <td>{!! $proyecto->ciudad !!}</td>
                          <td>{!! $proyecto->generador !!}</td>
            <td>{!! $proyecto->idempresa !!}</td>
            <td>
                 <a href="{{ route('proyectos.show', $proyecto->id) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view proyecto"></i>
                 </a>
                 <a href="{{ route('proyectos.edit', $proyecto->id) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit proyecto"></i>
                 </a>
                 <a href="{{ route('proyectos.confirm-delete', $proyecto->id) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete proyecto"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
 <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#proyectos-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#proyectos-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop