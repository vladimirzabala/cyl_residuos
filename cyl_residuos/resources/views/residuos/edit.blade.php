@extends('layouts/usuarios')

@section('title')
Residuos
@parent
@stop
@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Editar residuos</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('home') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                 Inicio
             </a>
         </li>
         <li>Residuos</li>
         <li class="active">Edit Residuo </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Editar  residuo
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($residuo, ['route' => ['residuos.update', $residuo->id], 'method' => 'patch']) !!}

        @include('residuos.fields')

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("form").submit(function() {
                $('input[type=submit]').attr('disabled', 'disabled');
                return true;
            });
        });
    </script>
@stop