<!-- Nombre Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>
<div class="form-group col-sm-12">
    {!! Form::label('tratamiento', 'Tratamiento:') !!}
    {!! Form::textarea('tratamiento', null, ['class' => 'form-control']) !!}
 
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('residuos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
