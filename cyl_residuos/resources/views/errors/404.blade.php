<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>404 página no encontrada | C&L</title>
      <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon.ico') }}" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- global level css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <!-- end of globallevel css-->
    <!-- page level styles-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/404.css') }}" />
    <!-- end of page level styles-->
</head>

<body>
    <div class="centrar">

            <div id="animate" class="row">
                <div class="number">4</div>
               <div class="number">0</div>
                <div class="number">4</div>
            </div>
            <div class="">
            <div class="hgroup">
                
         
                
            </div>
        </div>
<div style="left: 40px; position:fixed; top:10px">
<a href="https://www.car.gov.co"><img src="{{ asset('assets/images/mariposa.gif') }}" class="img-responsive" width="180px"></a>
</div>


</div>
    <!-- global js -->
    <script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!--livicons-->
    <script src="{{ asset('assets/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/js/livicons-1.4.min.js') }}"></script>
    <!-- end of global js -->
    <!-- begining of page level js-->
    <script src="{{ asset('assets/js/frontend/404.js') }}"></script>
    <!-- end of page level js-->
</body>
</html>