<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>C Y L Soluciones empresariales</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Vladimir Gómez Zabala">
 
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap-responsive.css') }}">
    
     <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}"> 
     <link rel="stylesheet" type="text/css" href="{{ asset('assets/color/default.css') }}">     
    <link rel="shortcut icon" href="{{ asset('assets/img/favicone.ico') }}">

</head>

<body>
    <!-- navbar -->
    <div class="navbar-wrapper">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="menu-custom-vladimir">
                <div class="container">
                    <!-- Responsive navbar -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </a>
                    <h6 class="brand animated flipInY"><a href="#"><img src="{{ asset('assets/img/logo_home_navbar.png') }}" alt="" width="240px"  /></a></h6>
                    <!-- navigation -->
                    <nav class="pull-right nav-collapse collapse">
                        <ul id="menu-main" class="nav">
                            <li><a title="team" href="#about">ACERCA DE</a></li>
                            <li><a title="services" href="#services">NUESTROS SERVICIOS</a></li>
                            <!--<li><a title="works" href="#works">TRABAJOS</a></li> -->
                    
                            <li><a title="contact" href="#contact">CONTÁCTENOS</a></li>
                            <li><a href="https://cylsolucionesempresariales.com/aulavirtual">AULA VIRTUAL</a></li>
                         <li>

                         
                          @if(Sentinel::guest())
                         <a href="{!! route('login') !!}">GESTIÓN DE RESIDUOS</a>
                       
                @else
                     <a href="{!! route('my-account') !!}">MI CUENTA</a>

                    @endif</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>


  <section class="spacer green">
     
  
   
<div id="myCarousel" class="carousel slide">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
                <div class="carousel-inner">
                  <div class="active item">
                    <img src="{{ asset('assets/img/slider1.jpeg') }}" alt="">
                 
                  </div>
                  <div class="item">
                    <img src="{{ asset('assets/img/slider2.jpg') }}" alt="">
                   
                  </div>
                  <div class="item">
                    
                     <img src="{{ asset('assets/img/slider1.jpeg') }}" alt="">
                  </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
                 
              </div>
                </section>
    <!-- end spacer section -->
    <!-- section: team -->
    <section id="about" class="section">
        <div class="container">
          
     <div class="row">
            <div class="span4" >
              <center>  
<div class="flyRight">
                                <img src="{{ asset('assets/images/quienes_somos.png') }}" alt="" width="215px" >
                                <br>
                                <p style="text-align: justify;font-size:14px;">
                               C&L SOLUCIONES EMPRESARIALES S.A.S., es una empresa que nace con el objetivo de asesorar e implementar las mejores estrategias en los diferentes sectores y empresas de la economía que buscan garantizar el cumplimiento de sus requisitos legales, ambientales y de seguridad y salud en el trabajo. Contamos con un grupo de profesionales especializados con amplia experiencia con el objetivo de satisfacer las necesidades de nuestros clientes.
                                <br>
                                <b>MISIÓN</b>
                                   <br>
                                Brindar de manera efectiva soluciones estratégicas en los diferentes campos del mercado nacional, a través de profesionales especializados, enfocados en satisfacer las necesidades del cliente y en la presentación de alternativas que mitiguen los riesgos de las organizaciones y garanticen su desarrollo sostenible, seguro y de calidad.
                                <br>
                               <b> VISIÓN</b>
                                  <br>
                                En el 2020 ser reconocidos en el mercado nacional como una firma que brinde soluciones estratégicas, oportunas y eficientes para el desarrollo sostenible, seguro y de calidad de cada organización.

            </p>
            </div>
            </center>
            </div>
            <div class="span8">
                  
            <div class="row">

                         <div class="span8">
                         <br><br><br>
                        <div id="portfolio-wrap">
                           
                          

                         @foreach($galeria as $imagen)   
                                <!-- portfolio item -->
                             
                                              <div class="">
                                                    <div class="" style="width:220px;">
                                                        <a href="{{asset($imagen->ruta) }}" data-pretty="prettyPhoto[gallery1]" class="portfolio-image">
                                                <img src="{{ asset($imagen->ruta) }}" alt="" />
                                                <div class="portfolio-overlay">
                                                    <div class="thumb-info">
                                                        <h5>{{$imagen->nombre }}</h5>
                                                        <i class="icon-plus icon-2x"></i>
                                                    </div>
                                                </div>
                                                </a>
                                                    </div>
                                                </div>
                               
                         
                                  
                                    
                                      
                                 <!-- end portfolio item -->
              

                       @endforeach
                          

                        </div>
                         
                </div>


     </div>
</div>
            
        </div>

        </div>
        <!-- /.container -->
    </section>
    <!-- end section: team -->
    <!-- section: services -->
    <section id="services" class="section orange">
        <div class="container">
             <div class="row">
            <div class="span4" >
              <center>  
<div class="flyLeft">
                                <img src="{{ asset('assets/images/nuestros_servicios.png') }}" alt="" width="215px">
                                <br>
             <p style="text-align: justify;font-size:14px;color:black;">
             La formación, competencias y experiencia profesional de nuestro equipo de trabajo en brindar soluciones estratégicas a los retos del mercado hace que podamos ofrecer los siguientes servicios : 
                

                </p>

                <ul style="text-align: justify;font-size:14px;color:black;margin-left:20px;">                   
            
           
            <li>   Capacitación, formación y entrenamiento en las diferentes necesidades de la organización (Auditores Internos, Formación de brigadas, temas de calidad, ambiente, Seguridad y Salud en el Trabajo, entre otros).</li> 

             <li>  Diseño y asesoría en planes de gestión de Residuos de Construcción y demolición, asesoría ante autoridades ambientales, Recolección y tratamiento de residuos de obra. </li>

             <li>  Consultoría ambiental y Recolección Residuos (LEED, evaluaciones de impacto ambiental, permisos y licencias ambiental, Planes Gestión Ambiental).</li>

               <li>  Consultoría en Seguridad y Salud en el Trabajo (Diseño SG-SST, análisis puestos de trabajo, baterías riesgo psicosocial, mediciones higiénicas, planes estratégicos seguridad vial, entre otros).</li>
           

           <li>Consultoría, asesoría y auditoria en Sistemas de Gestión ( SG-SST, ISO 9001, ISO 14001, OHSAS 18001, ISO 45001, ISO 22000, ISO 27001, ISO 50001, HACCP, GP 1000, MECI, RUC Y NORSOK)</li>      

           
          
              <li>  Suministros elementos ambientales y de seguridad ( Señalización, dotación, EPP, Productos de Aseo Biodegradables).</li>
            </ul>
            </div>
            </center>
            </div>
            <div class="span8">
                  
            <div class="row">

                         <div class="span8">
                         <br><br><br>
                        <div id="portfolio-wrap">
                           
                          

                        <div class="row">
                <div class="span2 animated-fast flyIn">
                    <div class="service-box">
                        <img src="{{ asset('assets/images/servicio1.png') }}" alt="" />
                       
                        
                    </div>
                </div>
                <div class="span2 animated flyIn">
                    <div class="service-box">
                        <img src="{{ asset('assets/images/servicio2.png') }}" alt="" />
                        
                        
                    </div>
                </div>
                <div class="span2 animated-fast flyIn">
                    <div class="service-box">
                        <img src="{{ asset('assets/images/servicio3.png') }}" alt="" />
                        
                       
                    </div>
                </div>
                <div class="span2 animated-slow flyIn">
                    <div class="service-box">
                        <img src="{{ asset('assets/images/servicio4.png') }}" alt="" />
                        
                        
                    </div>
                </div>
                <div class="span2 animated-slow flyIn">
                    <div class="service-box">
                        <img src="{{ asset('assets/images/servicio5.png') }}" alt="" />
                        
                        
                    </div>
                </div>
            </div>
                          

                        </div>
                         
                </div>


     </div>
</div>
            
        </div>
         
        </div>
    </section>
    <!-- end section: services -->
    <!-- section: works -->
  <!--<section id="works" class="section">
        <div class="container clearfix">
           

     <div class="row">
            <div class="span4">col-sm-8</div>
            <div class="span8">
                  
            <div class="row">

                         <div class="span8">
                        <div id="portfolio-wrap">
                           
                          

                    AQUI LOS TRABAJOS...
                          

                        </div>
                         
                </div>


     </div>
</div>
            
        </div>
        </div>
         </section>-->
    <!-- spacer section -->
  

    <section id="contact" class="section green">
        <div class="container">

                     <div class="row">
            <div class="span4 offset1" >
              <center>  
<div  class="flyLeft">
                                <img src="{{ asset('assets/images/contactenos.png') }}" alt="" >
                                <br>
                                <p style="text-align: left;color: black;" >
                                <b>Celular:</b> 311 5111005 - 321 7140711
                                <br>
                                <b>Correo Electrónico:</b>
                                 info@cylsolucionesempresariales.com<br>
                                <b>www.cylsolucionesempresariales.com</b>
                                <br>
                                Bogotá - Colombia
            </p>
            </div>
            </center>
            </div>
            <div class="span6">
                  
            <div class="row">

                <div class="span4 offset1">
                        
                










         
           
                    <div class="cform" id="contact-form">
                        <div id="sendmessage">Su mensaje ha sido enviado, Gracias!</div>
                        <div id="errormessage"></div>
                        <form action="" method="post" role="form" class="contactForm">
                            <div class="row">
                                <div class="span6">
                                    <div class="field your-name form-group">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="SU NOMBRE" data-rule="minlen:4" data-msg="Por favor ingrese al menos 4 caracteres" />
                                        <div class="validation"></div>
                                    </div>
                                    <div class="field your-email form-group">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="SU CORREO" data-rule="email" data-msg="Por favor introduzca una dirección de correo electrónico válida" />
                                        <div class="validation"></div>
                                    </div>
                                    <div class="field subject form-group">
                                        <input type="text" class="form-control" name="subject" id="subject" placeholder="ASUNTO" data-rule="minlen:4" data-msg="Por favor ingrese al menos 8 caracteres del tema" />
                                        <div class="validation"></div>
                                    </div>
                                       <div class="field message form-group">
                                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="
Por favor escribe algo para nosotros" placeholder="MENSAJE"></textarea>
                                        <div class="validation"></div>
                                    </div>
                                    <input type="image" value="Enviar mensaje" class="pull-left" src="{{ asset('assets/images/enviar_mensaje.png') }}" style="width: 200px">
                                </div>
                             
                            </div>
                        </form>
                    </div>
               














                         
                </div>


     </div>
</div>
            
        </div>



        </div>
    </section>
    <!--<footer>
        <div class="container">
            <div class="row">
                <div class="span6 offset3">
                    <ul class="social-networks">
                        <li><a href="#"><i class="icon-circled icon-bgdark icon-instagram icon-2x"></i></a></li>
                        <li><a href="#"><i class="icon-circled icon-bgdark icon-twitter icon-2x"></i></a></li>
                        <li><a href="#"><i class="icon-circled icon-bgdark icon-dribbble icon-2x"></i></a></li>
                        <li><a href="#"><i class="icon-circled icon-bgdark icon-pinterest icon-2x"></i></a></li>
                    </ul>
                    <p class="copyright">
                    
                        <div class="credits">
                    
                            Desarrollado por <a href="#">Vladimir Gómez</a>
                        </div>
                    </p>
                </div>
            </div>
        </div> -->
        <!-- ./container -->
    </footer>
    <a href="#" class="scrollup">
           <img src="{{ asset('assets/images/volver.png') }}" alt="" />
    </a>
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.scrollTo.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.nav.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.localScroll.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('assets/js/isotope.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
    <script src="{{ asset('assets/js/inview.js') }}"></script>
    <script src="{{ asset('assets/js/animate.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/contactform/contactform.js') }}"></script>

</body>

</html>
