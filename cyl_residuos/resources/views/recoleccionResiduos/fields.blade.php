<!-- Idempresa Field -->
<div class="form-group col-sm-12">
    {!! Form::label('idproyecto', 'Idproyecto (No. de proyecto):') !!}
    {!! Form::number('idproyecto', null, ['class' => 'form-control']) !!}
</div>

<!-- Residuo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('residuo', 'Residuo:') !!}  
    {!! Form::select('residuo', $residuos,null,['class' => 'form-control','placeholder'=>'Seleccione una opción...','required']) !!}
</div>

<!-- Cantidad Field -->
<div class="form-group col-sm-12">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    {!! Form::number('cantidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Unidad Field -->
<div class="form-group col-sm-12">
    {!! Form::label('unidad', 'Unidad:') !!}
    
        {!! Form::select('unidad', ['kilogramo' => 'kilogramo', 'metro' => 'metro', 'tonelada' => 'tonelada'], null, ['class' => 'form-control','placeholder' => 'Por favor seleccione el tipo de residuo']) !!}
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fecha', 'Fecha retiro:') !!}
    {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('recoleccionResiduos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
