@extends('layouts/usuarios')

@section('title')
Estadísticas residuos
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/animationcharts/jquery.circliful.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/pages/charts.css') }}" rel="stylesheet" type="text/css"/>
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Estadísticas </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('home') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Inicio
            </a>
        </li>
        <li>Estadísticas </li>
        <li class="active">Estadísticas residuos</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                   Estadísticas residuos
                </h4>
               
            </div>
            <br />




 

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- Stack charts strats here-->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="barchart" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i> Gráfico de barras
                        </h3>
                        <span class="pull-right">
                        <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                        <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                    </span>
                    </div>
                    <div class="panel-body">
                    




   {!! Form::open(['route' => 'graficobarras']) !!}   

                                 


            <div class="form-group col-sm-3">
                {!! Form::label('fechainicial', 'Seleccione el año:') !!}
                {!! Form::select('año', [
                '2018' => '2018',
                '2019' => '2019',
                '2020' => '2020',
                '2021' => '2021',
                '2022'=>'2022'],
                 null, ['class' => 'form-control']) !!}
                

            </div>
            <div class="form-group col-sm-3">
                {!! Form::label('fechafinal', 'Seleccione el mes:') !!}
                           
                {!! Form::select('mes', [
                '1' => 'ENERO',
                '2' => 'FEBRERO',
                '3' => 'MARZO',
                '4' => 'ABRIL',
                '5' => 'MAYO',
                '6' => 'JUNIO',
                '7' => 'JULIO',
                '8' => 'AGOSTO',
                '9' => 'SEPTIEMBRE',
                '10' => 'OCTUBRE',
                '11' => 'NOVIEMBRE',
                '12' => 'DICIEMBRE'],
                 null, ['class' => 'form-control']) !!}


                
            </div>

            <div class="form-group col-sm-3">
                <div class="col-sm-10">
                    {!! Form::label('id_proyecto', 'Seleccione el proyecto:') !!}
                    {!! Form::select('id_proyecto',$proyectosautorizados,null,['class' => 'form-control','placeholder'=>'Seleccione una opción...','required']) !!}
               </div>
        </div>

             
            <div class="form-group col-sm-2 text-center">
            <br>
                   

                      <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i> BUSCAR
                                </button>
            </div>
            <br>

 {!! Form::close() !!}

                
             
            

                    </div>
                </div>
            </div>


           
        </div>
      
     

    </section>





















          
        </div>
 </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')


@stop
