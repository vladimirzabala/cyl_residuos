<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $recoleccionResiduo->id !!}</p>
    <hr>
</div>

<!-- Idempresa Field -->
<div class="form-group">
    {!! Form::label('idempresa', 'Idempresa:') !!}
    <p>{!! $recoleccionResiduo->idempresa !!}</p>
    <hr>
</div>

<!-- Residuo Field -->
<div class="form-group">
    {!! Form::label('residuo', 'Residuo:') !!}
    <p>{!! $recoleccionResiduo->residuo !!}</p>
    <hr>
</div>

<!-- Cantidad Field -->
<div class="form-group">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    <p>{!! $recoleccionResiduo->cantidad !!}</p>
    <hr>
</div>

<!-- Unidad Field -->
<div class="form-group">
    {!! Form::label('unidad', 'Unidad:') !!}
    <p>{!! $recoleccionResiduo->unidad !!}</p>
    <hr>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $recoleccionResiduo->fecha !!}</p>
    <hr>
</div>

<div class="form-group">
    {!! Form::label('puntorecoleccion', 'puntorecoleccion:') !!}
    <p>{!! $recoleccionResiduo->puntorecoleccion !!}</p>
    <hr>
</div>
<div class="form-group">
    {!! Form::label('ciudad', 'ciudad:') !!}
    <p>{!! $recoleccionResiduo->ciudad !!}</p>
    <hr>
</div>
<div class="form-group">
    {!! Form::label('generador', 'Generador:') !!}
    <p>{!! $recoleccionResiduo->generador !!}</p>
    <hr>
</div>
<div class="form-group">
    {!! Form::label('direccion', 'DirecciÓn:') !!}
    <p>{!! $recoleccionResiduo->direccion !!}</p>
    <hr>
</div>
<div class="form-group">
    {!! Form::label('tratamiento', 'Tratamiento:') !!}
    <p>{!! $recoleccionResiduo->tratamiento !!}</p>
    <hr>
</div>

