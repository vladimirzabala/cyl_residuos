<table class="table table-responsive" id="recoleccionResiduos-table" width="100%">
    <thead>
     <tr>
        <th>No. de proyecto</th>
        <th>Residuo</th>
        <th>Cantidad</th>
        <th>Unidad</th>
        <th>Fecha recolección</th>
        <th>Fecha recolección sistema </th>
        <th >Acción</th>
     </tr>
    </thead>
    <tbody>
    @foreach($recoleccionResiduos as $recoleccionResiduo)
        <tr>
            <td>{!! $recoleccionResiduo->idproyecto !!}</td>
            <td>{!! $recoleccionResiduo->residuo !!}</td>
            <td>{!! $recoleccionResiduo->cantidad !!}</td>
            <td>{!! $recoleccionResiduo->unidad !!}</td>
            <td>{!! $recoleccionResiduo->fecha !!}</td>
            <td>{!! $recoleccionResiduo->created_at !!}</td>
            <td>
                 <a href="{{ route('recoleccionResiduos.show', $recoleccionResiduo->id) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="VER EN DETALLE DE ESTA RECOLECCIÓN"></i>
                 </a>
                 <a href="{{ route('recoleccionResiduos.edit', $recoleccionResiduo->id) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit recoleccionResiduo"></i>
                 </a>
                 <a href="{{ route('recoleccionResiduos.confirm-delete', $recoleccionResiduo->id) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete recoleccionResiduo"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
 <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#recoleccionResiduos-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#recoleccionResiduos-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop