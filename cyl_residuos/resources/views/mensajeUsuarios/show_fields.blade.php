<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $mensajeUsuarios->id !!}</p>
    <hr>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $mensajeUsuarios->nombre !!}</p>
    <hr>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefono:') !!}
    <p>{!! $mensajeUsuarios->telefono !!}</p>
    <hr>
</div>

<!-- Ciudad Field -->
<div class="form-group">
    {!! Form::label('ciudad', 'Ciudad:') !!}
    <p>{!! $mensajeUsuarios->ciudad !!}</p>
    <hr>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('Correo', 'Correo:') !!}
    <p>{!! $mensajeUsuarios->Correo !!}</p>
    <hr>
</div>

<!-- Tipoobra Field -->
<div class="form-group">
    {!! Form::label('tipoObra', 'Tipoobra:') !!}
    <p>{!! $mensajeUsuarios->tipoObra !!}</p>
    <hr>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('Direccion', 'Direccion:') !!}
    <p>{!! $mensajeUsuarios->Direccion !!}</p>
    <hr>
</div>

