<table class="table table-responsive" id="mensajeUsuarios-table" width="100%">
    <thead>
     <tr>
        <th>Nombre</th>
        <th>Telefono</th>
        <th>Ciudad</th>
        <th>Correo</th>
        <th>Tipoobra</th>
        <th>Direccion</th>
        <th >Acción</th>
     </tr>
    </thead>
    <tbody>
    @foreach($mensajeUsuarios as $mensajeUsuarios)
        <tr>
            <td>{!! $mensajeUsuarios->nombre !!}</td>
            <td>{!! $mensajeUsuarios->telefono !!}</td>
            <td>{!! $mensajeUsuarios->ciudad !!}</td>
            <td>{!! $mensajeUsuarios->Correo !!}</td>
            <td>{!! $mensajeUsuarios->tipoObra !!}</td>
            <td>{!! $mensajeUsuarios->Direccion !!}</td>
            <td>
                 <a href="{{ route('mensajeUsuarios.show', $mensajeUsuarios->id) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view mensajeUsuarios"></i>
                 </a>
                 <a href="{{ route('mensajeUsuarios.edit', $mensajeUsuarios->id) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit mensajeUsuarios"></i>
                 </a>
                 <a href="{{ route('mensajeUsuarios.confirm-delete', $mensajeUsuarios->id) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete mensajeUsuarios"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
 <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#mensajeUsuarios-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#mensajeUsuarios-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop