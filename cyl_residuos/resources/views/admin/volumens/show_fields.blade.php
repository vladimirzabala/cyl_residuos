<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $volumen->id !!}</p>
    <hr>
</div>

<!-- Anio Field -->
<div class="form-group">
    {!! Form::label('Anio', 'Anio:') !!}
    <p>{!! $volumen->Anio !!}</p>
    <hr>
</div>

<!-- Imagen Field -->
<div class="form-group">
    {!! Form::label('Imagen', 'Imagen:') !!}
    <p>{!! $volumen->Imagen !!}</p>
    <hr>
</div>

<!-- Numero Field -->
<div class="form-group">
    {!! Form::label('Numero', 'Numero:') !!}
    <p>{!! $volumen->Numero !!}</p>
    <hr>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('Nombre', 'Nombre:') !!}
    <p>{!! $volumen->Nombre !!}</p>
    <hr>
</div>

<!-- Issn Field -->
<div class="form-group">
    {!! Form::label('Issn', 'Issn:') !!}
    <p>{!! $volumen->Issn !!}</p>
    <hr>
</div>

<!-- Resumen Field -->
<div class="form-group">
    {!! Form::label('Resumen', 'Resumen:') !!}
    <p>{!! $volumen->Resumen !!}</p>
    <hr>
</div>

<!-- Ideditor Field -->
<div class="form-group">
    {!! Form::label('IdEditor', 'Ideditor:') !!}
    <p>{!! $volumen->IdEditor !!}</p>
    <hr>
</div>

<!-- Idusuario Field -->
<div class="form-group">
    {!! Form::label('IdUsuario', 'Idusuario:') !!}
    <p>{!! $volumen->IdUsuario !!}</p>
    <hr>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('Fecha', 'Fecha:') !!}
    <p>{!! $volumen->Fecha !!}</p>
    <hr>
</div>

<!-- Idestado Field -->
<div class="form-group">
    {!! Form::label('IdEstado', 'Idestado:') !!}
    <p>{!! $volumen->IdEstado !!}</p>
    <hr>
</div>

