<!-- Anio Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Anio', 'Anio:') !!}
    {!! Form::text('Anio', null, ['class' => 'form-control']) !!}
</div>

<!-- Imagen Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Imagen', 'Imagen:') !!}
    {!! Form::text('Imagen', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Numero', 'Numero:') !!}
    {!! Form::text('Numero', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Nombre', 'Nombre:') !!}
    {!! Form::text('Nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Issn Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Issn', 'Issn:') !!}
    {!! Form::text('Issn', null, ['class' => 'form-control']) !!}
</div>

<!-- Resumen Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Resumen', 'Resumen:') !!}
    {!! Form::text('Resumen', null, ['class' => 'form-control']) !!}
</div>

<!-- Ideditor Field -->
<div class="form-group col-sm-12">
    {!! Form::label('IdEditor', 'Ideditor:') !!}
    {!! Form::text('IdEditor', null, ['class' => 'form-control']) !!}
</div>

<!-- Idusuario Field -->
<div class="form-group col-sm-12">
    {!! Form::label('IdUsuario', 'Idusuario:') !!}
    {!! Form::text('IdUsuario', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Fecha', 'Fecha:') !!}
    {!! Form::text('Fecha', null, ['class' => 'form-control']) !!}
</div>

<!-- Idestado Field -->
<div class="form-group col-sm-12">
    {!! Form::label('IdEstado', 'Idestado:') !!}
    {!! Form::number('IdEstado', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.volumens.index') !!}" class="btn btn-default">Cancel</a>
</div>
