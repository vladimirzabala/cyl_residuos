<table class="table table-responsive" id="volumens-table" width="100%">
    <thead>
     <tr>
        <th>Anio</th>
        <th>Imagen</th>
        <th>Numero</th>
        <th>Nombre</th>
        <th>Issn</th>
        <th>Resumen</th>
        <th>Ideditor</th>
        <th>Idusuario</th>
        <th>Fecha</th>
        <th>Idestado</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($volumens as $volumen)
        <tr>
            <td>{!! $volumen->Anio !!}</td>
            <td>{!! $volumen->Imagen !!}</td>
            <td>{!! $volumen->Numero !!}</td>
            <td>{!! $volumen->Nombre !!}</td>
            <td>{!! $volumen->Issn !!}</td>
            <td>{!! $volumen->Resumen !!}</td>
            <td>{!! $volumen->IdEditor !!}</td>
            <td>{!! $volumen->IdUsuario !!}</td>
            <td>{!! $volumen->Fecha !!}</td>
            <td>{!! $volumen->IdEstado !!}</td>
            <td>
                 <a href="{{ route('admin.volumens.show', $volumen->id) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view volumen"></i>
                 </a>
                 <a href="{{ route('admin.volumens.edit', $volumen->id) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit volumen"></i>
                 </a>
                 <a href="{{ route('admin.volumens.confirm-delete', $volumen->id) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete volumen"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#volumens-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#volumens-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop