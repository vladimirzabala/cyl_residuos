<ul id="menu" class="page-sidebar-menu">

    <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
        <a href="{{ route('admin.dashboard') }}">
            <i class="livicon" data-name="dashboard" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Dashboard 1</span>
        </a>
    </li>
  

   
 
    <li {!! (Request::is('admin/activity_log') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/activity_log') }}">
            <i class="livicon" data-name="eye-open" data-size="18" data-c="#F89A14" data-hc="#F89A14"
               data-loop="true"></i>
            Log de actividades
        </a>
    </li>
   
  

   
  
 
  
    
    <li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Usuarios</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
          <li {!! ((Request::is('admin/users/*')) && !(Request::is('admin/users/create')) ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::route('admin.users.show',Sentinel::getUser()->id) }}">
                    <i class="fa fa-angle-double-right"></i>
                   Mi perfil
                </a>
            </li>
            <li {!! (Request::is('admin/users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver usuarios 
                </a>
            </li>
            <li {!! (Request::is('admin/users/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar Usuario
                </a>
            </li>
          
            <li {!! (Request::is('admin/deleted_users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Usuarios eliminados
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/groups') || Request::is('admin/groups/create') || Request::is('admin/groups/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="users" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Roles</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/groups') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/groups') }}">
                    <i class="fa fa-angle-double-right"></i>
                   Lista de roles
                </a>
            </li>
            <li {!! (Request::is('admin/groups/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/groups/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Nuevo rol
                </a>
            </li>
        </ul>
    </li>
   
  <li class="{{ Request::is('admin/clientes*') ? 'active' : '' }}">
    <a href="{!! route('admin.clientes.index') !!}">
    <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="users-add" data-size="18"
               data-loop="true"></i>
              Asignar empresas a usuarios 
                <span class="fa arrow"></span>
    </a>

     <ul class="sub-menu">
            <li {!! (Request::is('admin/groups') ? 'class="active" id="active"' : '') !!}>
                <a href="{!! route('admin.clientes.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                   Asignar
                </a>
            </li>
            <li {!! (Request::is('admin/groups/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{!! route('admin.clientes.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver asignaciones
                </a>
            </li>
        </ul>

      


       
</li>


 <li>
    <a href="{!! route('admin.usuariosProyectos.index') !!}">
    <i class="livicon" data-c="#F89A14" data-hc="#F89A14" data-name="folder-add" data-size="18"
               data-loop="true"></i>
              Asignar proyectos
                <span class="fa arrow"></span>
    </a>

  <ul class="sub-menu">
             <li class="{{ Request::is('admin/usuariosProyectos*') ? 'active' : '' }}">
               <a href="{!! route('admin.usuariosProyectos.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                   Asignar
                </a>
            </li>
            <li {!! (Request::is('admin/groups/create') ? 'class="active" id="active"' : '') !!}>
                 <a href="{!! route('admin.usuariosProyectos.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver asignaciones
                </a>
            </li>
        </ul>
   </li>



   
</ul>