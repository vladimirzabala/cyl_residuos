



<li class="{{ Request::is('admin/residuos*') ? 'active' : '' }}">
    <a href="{!! route('admin.residuos.index') !!}">
    <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="$ICON_NAME$" data-size="18"
               data-loop="true"></i>
               Residuos
    </a>
</li>

<li class="{{ Request::is('admin/recoleccionResiduos*') ? 'active' : '' }}">
    <a href="{!! route('admin.recoleccionResiduos.index') !!}">
    <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="$ICON_NAME$" data-size="18"
               data-loop="true"></i>
               Recoleccion_residuos
    </a>
</li>

<li class="{{ Request::is('admin/unidades*') ? 'active' : '' }}">
    <a href="{!! route('admin.unidades.index') !!}">
    <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="$ICON_NAME$" data-size="18"
               data-loop="true"></i>
               Unidades
    </a>
</li>

<li class="{{ Request::is('admin/clientes*') ? 'active' : '' }}">
    <a href="{!! route('admin.clientes.index') !!}">
    <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="$ICON_NAME$" data-size="18"
               data-loop="true"></i>
               Clientes
    </a>
</li>

<li class="{{ Request::is('admin/usuariosProyectos*') ? 'active' : '' }}">
    <a href="{!! route('admin.usuariosProyectos.index') !!}">
    <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="$ICON_NAME$" data-size="18"
               data-loop="true"></i>
               Usuarios_proyectos
    </a>
</li>

