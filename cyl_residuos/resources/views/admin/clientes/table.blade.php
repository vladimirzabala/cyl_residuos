<table class="table table-responsive" id="clientes-table" width="100%">
    <thead>
     <tr>
        <th>No. Asignación</th>
    
        <th>Empresa</th>
        <th>Usuario</th>
        <th >Acción</th>
     </tr>
    </thead>
    <tbody>
    @foreach($clientes as $cliente)
        <tr>
            <td>{!! $cliente->id !!}</td>
           
            <td>{!! $cliente->nombre !!}</td>
            <td>{!! $cliente->first_name !!}&nbsp;{!! $cliente->last_name !!}</td>
            <td>
                 
                
                 <a href="{{ route('admin.clientes.confirm-delete', $cliente->id) }}" data-toggle="modal" data-target="#delete_confirm">Eliminar asignación
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="Eliminar asignación"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#clientes-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#clientes-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop