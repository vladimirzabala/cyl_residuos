<!-- Idempresa Field -->

<div class="form-group col-sm-12">
   
    

     <div class="col-sm-10">
      {!! Form::label('idEmpresa', 'Empresa:') !!}
        {!! Form::select('idEmpresa', $empresas, null,['class' => 'form-control select2', 'id' => 'empresas']) !!}
     </div>
</div>

<!-- Idusuario Field -->

<div class="form-group col-sm-12">
   
   
        <div class="col-sm-10">
         {!! Form::label('idUsuario', 'Usuario:') !!}
        {!! Form::select('idUsuario', $usuarios, null,['class' => 'form-control select2', 'id' => 'usuarios']) !!}
     </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.clientes.index') !!}" class="btn btn-default">Cancelar</a>
</div>

