@extends('admin/layouts/default')

@section('title')
Asignaciones de proyectos
@parent
@stop
@section('header_styles')
    <!--page level css -->
  
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
   
    <!--end of page level css-->
@stop

@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>Permisos proyectos</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Usuarios proyectos</li>
        <li class="active">Asignar proyectos a usuarios</li>
    </ol>
</section>
<section class="content paddingleft_right15">
<div class="row">
 <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Asignar proyectos a usuarios
            </h4></div>
        <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'admin.usuariosProyectos.store']) !!}

            @include('admin.usuariosProyectos.fields')

        {!! Form::close() !!}
    </div>
  </div>
 </div>
</section>
 @stop
@section('footer_scripts')

    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form").submit(function() {
                $('input[type=submit]').attr('disabled', 'disabled');
                return true;
            });
   



         function formatProyectos (proyecto) {
            if (!proyecto.id) { return proyecto.text; }
            var $proyecto = $(
                '<span> ' + proyecto.text + '</span>'
            );
            return $proyecto;

        }
        $("#proyectos").select2({
            templateResult: formatProyectos,
            templateSelection: formatProyectos,
            placeholder: "seleccione una proyecto",
            theme:"bootstrap"
        });


  function formatUsuarios (usuario) {
            if (!usuario.id) { return usuario.text; }
            var $usuario = $(
                '<span> ' + usuario.text + '</span>'
            );
            return $usuario;

        }
        $("#usuarios").select2({
            templateResult: formatUsuarios,
            templateSelection: formatUsuarios,
            placeholder: "seleccione un usuario",
            theme:"bootstrap"
        });



        });
    </script>
@stop
