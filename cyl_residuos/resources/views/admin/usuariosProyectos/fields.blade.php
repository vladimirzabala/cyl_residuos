<!-- Id Empresa Field -->
<div class="form-group col-sm-12">
       <div class="col-sm-10">
      {!! Form::label('id_proyecto', 'Proyecto:') !!}
        {!! Form::select('id_proyecto', $proyectos, null,['class' => 'form-control select2', 'id' => 'proyectos']) !!}
     </div>
</div>

<!-- Id Usuario Field -->
<div class="form-group col-sm-12">
  <div class="col-sm-10">
         {!! Form::label('Id_usuario', 'Usuario:') !!}
        {!! Form::select('Id_usuario', $usuarios, null,['class' => 'form-control select2', 'id' => 'usuarios']) !!}
     </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.usuariosProyectos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
