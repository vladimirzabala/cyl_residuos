<ul id="menu" class="page-sidebar-menu">    
       
  
   @if(!Sentinel::guest())



  
 
  
 @if(Sentinel::inRole('user'))





 <li class="{{ Request::is('admin/uploads*') ? 'active' : '' }}">
       <a href="{!! route('uploads.index') !!}">
                <i class="livicon" data-name="users" data-c="#F89A14" data-hc="#F89A14" data-size="18"
               data-loop="true"></i>
              
            <span class="title">Empresas</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                 <a href="{!! route('empresas.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar           </a>
            </li>
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                <a href="{!! route('empresas.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver empresas
                </a>
            </li>
        </ul>
    </li>

     <li class="{{ Request::is('admin/uploads*') ? 'active' : '' }}">
       <a href="{!! route('proyectos.index') !!}">
                <i class="livicon" data-name="briefcase" data-c="#F89A14" data-hc="#F89A14" data-size="21"
               data-loop="true"></i>
              
            <span class="title">Proyectos</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                 <a href="{!! route('proyectos.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar           </a>
            </li>
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                <a href="{!! route('proyectos.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver proyectos
                </a>
            </li>
        </ul>
    </li>

      <li class="{{ Request::is('admin/uploads*') ? 'active' : '' }}">
       <a href="{!! route('residuos.index') !!}">
                <i class="livicon" data-name="recycled" data-c="#F89A14" data-hc="#F89A14" data-size="22"
               data-loop="true"></i>
              
            <span class="title">Residuos</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                 <a href="{!! route('residuos.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar           </a>
            </li>
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                <a href="{!! route('residuos.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver residuos
                </a>
            </li>
        </ul>
    </li>


     <li class="{{ Request::is('admin/uploads*') ? 'active' : '' }}">
       <a href="{!! route('recoleccionResiduos.index') !!}">
                <i class="livicon" data-name="truck" data-c="#F89A14" data-hc="#F89A14" data-size="22"
               data-loop="true"></i>
              
            <span class="title">Recolección de residuos</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                 <a href="{!! route('recoleccionResiduos.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar           </a>
            </li>
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                <a href="{!! route('recoleccionResiduos.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver recolecciones
                </a>
            </li>
        </ul>
    </li>


  <li class="{{ Request::is('admin/uploads*') ? 'active' : '' }}">
       <a href="{!! route('uploads.index') !!}">
                <i class="livicon" data-name="film" data-c="#F89A14" data-hc="#F89A14" data-size="18"
               data-loop="true"></i>
            <span class="title">Subir multimedia</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                 <a href="{!! route('uploads.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar           </a>
            </li>
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                <a href="{!! route('uploads.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver archivos
                </a>
            </li>
        </ul>
    </li>

 <li class="{{ Request::is('admin/uploads*') ? 'active' : '' }}">
       <a href="{!! route('uploads.index') !!}">
                <i class="livicon" data-name="image" data-c="#F89A14" data-hc="#F89A14" data-size="18"
               data-loop="true"></i>
              
            <span class="title">Galeria</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                 <a href="{!! route('galeria.create') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Agregar imagenes           </a>
            </li>
            <li class="{{ Request::is('uploads*') ? 'active' : '' }}">
                <a href="{!! route('galeria.index') !!}">
                    <i class="fa fa-angle-double-right"></i>
                    Ver galeria
                </a>
            </li>
        </ul>
    </li>





    

<li class="{{ Request::is('admin/mensajeUsuarios*') ? 'active' : '' }}">
    <a href="{!! route('mensajeUsuarios.index') !!}">
     <i class="livicon" data-name="message-flag" data-c="#F89A14" data-hc="#F89A14" data-size="20"
               data-loop="true"></i>
               Mensajes Usuarios
    </a>
</li>


@endif

 @if(Sentinel::inRole('cliente'))


 <li class="{{ Request::is('admin/contenidos*') ? 'active' : '' }}"">
                            <a href="{!! route('estadisticasresiduos') !!}">
                                         
                           <i class="livicon" data-name="presentation" data-c="#F89A14" data-hc="#F89A14" data-size="24"
               data-loop="true"></i>
                                <span class="title">Estadísticas</span>
                       
                            </a>
                         
                        </li>

                         <li class="{{ Request::is('admin/contenidos*') ? 'active' : '' }}"">
                            <a href="{!! route('certificado') !!}">
                                         
                                    <i class="livicon" data-name="medal" data-c="#F89A14" data-hc="#F89A14" data-size="24"
               data-loop="true"></i>
                                <span class="title">Certificados</span>
                                
                            </a>
                          
                        </li>

                         <li class="{{ Request::is('admin/contenidos*') ? 'active' : '' }}"">
                            <a href="{!! route('reporteexcel') !!}">
                                         
                                    <i class="livicon" data-name="download" data-c="#F89A14" data-hc="#F89A14" data-size="24"
               data-loop="true"></i>
                                <span class="title">Reporte en excel</span>
                                
                            </a>
                          
                        </li>
 @endif



@endif


  <li style="height: 400px;" class="{{ Request::is('banners*') ? 'active' : '' }}">
  </li>

     
</ul>