<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Upload extends Model
{

    public $table = 'uploads';
    


    public $fillable = [
        'filename',
        'tipo',
        'descripcion',
        'sitio'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'filename' => 'string',
        'tipo' => 'string',
        'descripcion' => 'string',
        'sitio' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tipo' => 'required',
        'descripcion'=>'required'
        
    ];
}
