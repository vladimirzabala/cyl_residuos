<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Usuarios_proyecto extends Model
{

    public $table = 'usuarios_proyectos';
    


    public $fillable = [
        'id_proyecto',
        'Id_usuario'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'Id_usuario' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_proyecto' => 'required',
        'Id_usuario' => 'required'
    ];
}
