<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Residuo extends Model
{

    public $table = 'residuos';
    


    public $fillable = [
        'nombre',
        'descripcion',
        'tratamiento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'descripcion' => 'string',
        'tratamiento' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'descripcion' => 'required',
        'tratamiento' =>'required'
    ];
}
