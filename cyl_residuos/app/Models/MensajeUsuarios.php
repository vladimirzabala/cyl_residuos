<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class MensajeUsuarios extends Model
{

    public $table = 'mensaje_usuarios';
    


    public $fillable = [
        'nombre',
        'telefono',
        'ciudad',
        'Correo',
        'tipoObra',
        'Direccion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'telefono' => 'string',
        'ciudad' => 'string',
        'Correo' => 'string',
        'tipoObra' => 'string',
        'Direccion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'telefono' => 'required',
        'ciudad' => 'required',
        'Correo' => 'required|email',
        'tipoObra' => 'required'
    ];
}
