<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Cliente extends Model
{

    public $table = 'clientes';
    


    public $fillable = [
        'idEmpresa',
        'idUsuario'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idEmpresa' => 'integer',
        'idUsuario' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'idEmpresa' => 'integer',
        'idUsuario' => 'integer'
    ];
}
