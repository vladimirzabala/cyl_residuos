<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Menu extends Model
{
    use SoftDeletes;

    public $table = 'menus';
    

  


    public $fillable = [
        'nombre',
        'destino',
        'menupadre',
        'orden',
        'activo',
        'sitio'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'destino' => 'string',
        'menupadre' => 'integer',
        'activo' => 'boolean',
        'sitio' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'destino' => 'required',
        'menupadre' => 'integer',
        'orden' => 'integer',
        'activo' => 'boolean',
        'sitio'=>'string'
    ];


  
}
