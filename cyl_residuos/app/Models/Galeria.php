<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Galeria extends Model
{

    public $table = 'galeria';
    


    public $fillable = [
        'nombre',
        'ruta',
        'peso'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'ruta' => 'string',
        'peso' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'ruta' => 'required',
        'peso' => 'required'
    ];
}
