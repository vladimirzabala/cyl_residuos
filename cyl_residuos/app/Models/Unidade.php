<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Unidade extends Model
{

    public $table = 'unidades';
    


    public $fillable = [
        'Nombre',
        'Descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Nombre' => 'string',
        'Descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Nombre' => 'required'
    ];
}
