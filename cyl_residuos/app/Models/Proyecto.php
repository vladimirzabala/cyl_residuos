<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Proyecto extends Model
{

    public $table = 'proyectos';
    


    public $fillable = [
        'nombre',
        'descripcion',
        'idempresa',
        'puntorecoleccion',
        'ciudad',
        'direccion',
        'generador'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'descripcion' => 'string',
        'idempresa' => 'integer',
        'puntorecoleccion' => 'string',
        'ciudad' => 'string',
        'dirección' => 'string',
        'generador' => 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'idempresa' => 'required',
        'puntorecoleccion' => 'required',
        'ciudad' => 'required',
        'direccion' => 'required',
        'generador' => 'required'

    ];
}
