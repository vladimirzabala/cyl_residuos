<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Empresa extends Model
{

    public $table = 'empresas';
    


    public $fillable = [
        'nombre',
        'nit',
        'tipo',
        'direccion',
        'telefono'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'nit' => 'string',
        'tipo' => 'string',
        'direccion' => 'string',
        'telefono' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'nit' => 'required',
        'tipo' => 'required',
        'direccion' => 'required',
        'telefono' => 'required'
    ];
}
