<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Recoleccion_residuo extends Model
{

    public $table = 'recoleccion_residuos';
    


    public $fillable = [
        'idproyecto',
        'residuo',
        'cantidad',
        'unidad',
        'fecha'
       
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'idproyecto' => 'integer',
        'residuo' => 'integer',
        'cantidad' => 'integer',
        'unidad' => 'string'
              
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'idproyecto' => 'required|integer',
        'residuo' => 'required|integer',
        'cantidad' => 'required|integer',
        'unidad' => 'required|required',
        'fecha' => 'date|required'

    ];
}
