<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Archivo_certificados extends Model
{

    public $table = 'archivo_certificados';
    


    public $fillable = [
        'datos_proyecto',
        'residuos',
        'id_usuario',
        'codigo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'datos_proyecto' => 'string',
        'residuos' => 'string',
        'id_usuario' => 'integer',
        'codigo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'datos_proyecto' => 'required',
        'residuos' => 'required'
    ];
}
