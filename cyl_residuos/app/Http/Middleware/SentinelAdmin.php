<?php

namespace App\Http\Middleware;


use Closure;
use Sentinel;

class SentinelAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // SI NO ESTA CONECTADO
        if(!Sentinel::check())
            return redirect('admin/signin')->with('info', 'Usted debe estar conectado!');
        // SI EL ROL ES DIFERENTE DE ADMINISTRADOR
        elseif(!Sentinel::inRole('admin'))
            return redirect('my-account')->with('info', 'Usted no esta autorizado para esta sección');;


      

        return $next($request);
    }
}
