<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUnidadeRequest;
use App\Http\Requests\UpdateUnidadeRequest;
use App\Repositories\UnidadeRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Unidade;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UnidadeController extends InfyOmBaseController
{
    /** @var  UnidadeRepository */
    private $unidadeRepository;

    public function __construct(UnidadeRepository $unidadeRepo)
    {
        $this->unidadeRepository = $unidadeRepo;
    }

    /**
     * Display a listing of the Unidade.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->unidadeRepository->pushCriteria(new RequestCriteria($request));
        $unidades = $this->unidadeRepository->all();
        return view('admin.unidades.index')
            ->with('unidades', $unidades);
    }

    /**
     * Show the form for creating a new Unidade.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.unidades.create');
    }

    /**
     * Store a newly created Unidade in storage.
     *
     * @param CreateUnidadeRequest $request
     *
     * @return Response
     */
    public function store(CreateUnidadeRequest $request)
    {
        $input = $request->all();

        $unidade = $this->unidadeRepository->create($input);

        Flash::success('Unidade saved successfully.');

        return redirect(route('admin.unidades.index'));
    }

    /**
     * Display the specified Unidade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $unidade = $this->unidadeRepository->findWithoutFail($id);

        if (empty($unidade)) {
            Flash::error('Unidade not found');

            return redirect(route('unidades.index'));
        }

        return view('admin.unidades.show')->with('unidade', $unidade);
    }

    /**
     * Show the form for editing the specified Unidade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $unidade = $this->unidadeRepository->findWithoutFail($id);

        if (empty($unidade)) {
            Flash::error('Unidade not found');

            return redirect(route('unidades.index'));
        }

        return view('admin.unidades.edit')->with('unidade', $unidade);
    }

    /**
     * Update the specified Unidade in storage.
     *
     * @param  int              $id
     * @param UpdateUnidadeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnidadeRequest $request)
    {
        $unidade = $this->unidadeRepository->findWithoutFail($id);

        

        if (empty($unidade)) {
            Flash::error('Unidade not found');

            return redirect(route('unidades.index'));
        }

        $unidade = $this->unidadeRepository->update($request->all(), $id);

        Flash::success('Unidade updated successfully.');

        return redirect(route('admin.unidades.index'));
    }

    /**
     * Remove the specified Unidade from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.unidades.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Unidade::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.unidades.index'))->with('success', Lang::get('message.success.delete'));

       }

}
