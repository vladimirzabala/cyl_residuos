<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMensajeUsuariosRequest;
use App\Http\Requests\UpdateMensajeUsuariosRequest;
use App\Repositories\MensajeUsuariosRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\MensajeUsuarios;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MensajeUsuariosController extends InfyOmBaseController
{
    /** @var  MensajeUsuariosRepository */
    private $mensajeUsuariosRepository;

    public function __construct(MensajeUsuariosRepository $mensajeUsuariosRepo)
    {
        $this->mensajeUsuariosRepository = $mensajeUsuariosRepo;
    }

    /**
     * Display a listing of the MensajeUsuarios.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->mensajeUsuariosRepository->pushCriteria(new RequestCriteria($request));
        $mensajeUsuarios = $this->mensajeUsuariosRepository->all();
        return view('mensajeUsuarios.index')
            ->with('mensajeUsuarios', $mensajeUsuarios);
    }

    /**
     * Show the form for creating a new MensajeUsuarios.
     *
     * @return Response
     */
    public function create()
    {
        return view('mensajeUsuarios.create');
    }

    /**
     * Store a newly created MensajeUsuarios in storage.
     *
     * @param CreateMensajeUsuariosRequest $request
     *
     * @return Response
     */
    public function store(CreateMensajeUsuariosRequest $request)
    {
        $input = $request->all();

        $mensajeUsuarios = $this->mensajeUsuariosRepository->create($input);

        Flash::success('MensajeUsuarios saved successfully.');

        return redirect(route('mensajeUsuarios.index'));
    }

    /**
     * Display the specified MensajeUsuarios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mensajeUsuarios = $this->mensajeUsuariosRepository->findWithoutFail($id);

        if (empty($mensajeUsuarios)) {
            Flash::error('MensajeUsuarios not found');

            return redirect(route('mensajeUsuarios.index'));
        }

        return view('mensajeUsuarios.show')->with('mensajeUsuarios', $mensajeUsuarios);
    }

    /**
     * Show the form for editing the specified MensajeUsuarios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mensajeUsuarios = $this->mensajeUsuariosRepository->findWithoutFail($id);

        if (empty($mensajeUsuarios)) {
            Flash::error('MensajeUsuarios not found');

            return redirect(route('mensajeUsuarios.index'));
        }

        return view('mensajeUsuarios.edit')->with('mensajeUsuarios', $mensajeUsuarios);
    }

    /**
     * Update the specified MensajeUsuarios in storage.
     *
     * @param  int              $id
     * @param UpdateMensajeUsuariosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMensajeUsuariosRequest $request)
    {
        $mensajeUsuarios = $this->mensajeUsuariosRepository->findWithoutFail($id);

        

        if (empty($mensajeUsuarios)) {
            Flash::error('MensajeUsuarios not found');

            return redirect(route('mensajeUsuarios.index'));
        }

        $mensajeUsuarios = $this->mensajeUsuariosRepository->update($request->all(), $id);

        Flash::success('MensajeUsuarios updated successfully.');

        return redirect(route('mensajeUsuarios.index'));
    }

    /**
     * Remove the specified MensajeUsuarios from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('mensajeUsuarios.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = MensajeUsuarios::destroy($id);

           // Redirect to the group management page
           return redirect(route('mensajeUsuarios.index'))->with('success', Lang::get('message.success.delete'));

       }

}
