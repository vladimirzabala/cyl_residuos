<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateProyectoRequest;
use App\Http\Requests\UpdateProyectoRequest;
use App\Repositories\ProyectoRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Proyecto;
use App\Models\Recoleccion_residuo;
use App\Models\Usuarios_proyecto;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProyectoController extends InfyOmBaseController
{
    /** @var  ProyectoRepository */
    private $proyectoRepository;

    public function __construct(ProyectoRepository $proyectoRepo)
    {
        $this->proyectoRepository = $proyectoRepo;
    }

    /**
     * Display a listing of the Proyecto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->proyectoRepository->pushCriteria(new RequestCriteria($request));
        $proyectos = $this->proyectoRepository->all();
        return view('proyectos.index')
            ->with('proyectos', $proyectos);
    }

    /**
     * Show the form for creating a new Proyecto.
     *
     * @return Response
     */
    public function create()
    {
        return view('proyectos.create');
    }

    /**
     * Store a newly created Proyecto in storage.
     *
     * @param CreateProyectoRequest $request
     *
     * @return Response
     */
    public function store(CreateProyectoRequest $request)
    {
        $input = $request->all();

        $proyecto = $this->proyectoRepository->create($input);

        Flash::success('Proyecto guardado exitosamente.');

        return redirect(route('proyectos.index'));
    }

    /**
     * Display the specified Proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $proyecto = $this->proyectoRepository->findWithoutFail($id);

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        return view('proyectos.show')->with('proyecto', $proyecto);
    }

    /**
     * Show the form for editing the specified Proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $proyecto = $this->proyectoRepository->findWithoutFail($id);

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        return view('proyectos.edit')->with('proyecto', $proyecto);
    }

    /**
     * Update the specified Proyecto in storage.
     *
     * @param  int              $id
     * @param UpdateProyectoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProyectoRequest $request)
    {
        $proyecto = $this->proyectoRepository->findWithoutFail($id);

        

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        $proyecto = $this->proyectoRepository->update($request->all(), $id);

        Flash::success('Proyecto modificado exitosamente.');

        return redirect(route('proyectos.index'));
    }

    /**
     * Remove the specified Proyecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('proyectos.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
        // consultar si existen residuos recolectados los cuales esten asociados a ese proyecto
          $recoleccion_residuos = Recoleccion_residuo::where('idproyecto', $id)->first();

         

            // En caso de no estar asociado a una recolección de residuo
           if (empty($recoleccion_residuos)) {
            // Verificar que el proyecto no este asociado a un usuario
              $usuarios_proyectos = Usuarios_proyecto::where('id_proyecto', $id)->first();


                    if (empty($usuarios_proyectos)) {

                        // Eliminar el proyecto
                        $sample = Proyecto::destroy($id);
                        // redirigir a la vista principal de proyectos
                        return redirect(route('proyectos.index'))->with('success', Lang::get('message.success.delete'));
                    }
                    else
                    {

                        Flash::error('El proyecto no se puede eliminar ya que está asignado a un usuario.');
                         return redirect(route('proyectos.index'));

                    }
            
           }
           else{

            Flash::error('El proyecto no se puede eliminar ya que está asociado a una recoleccion de residuo.');
            return redirect(route('proyectos.index'));


           }


         

       }

}
