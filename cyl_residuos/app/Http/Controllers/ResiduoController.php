<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateResiduoRequest;
use App\Http\Requests\UpdateResiduoRequest;
use App\Repositories\ResiduoRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Residuo;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ResiduoController extends InfyOmBaseController
{
    /** @var  ResiduoRepository */
    private $residuoRepository;

    public function __construct(ResiduoRepository $residuoRepo)
    {
        $this->residuoRepository = $residuoRepo;
    }

    /**
     * Display a listing of the Residuo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->residuoRepository->pushCriteria(new RequestCriteria($request));
        $residuos = $this->residuoRepository->all();
        return view('residuos.index')
            ->with('residuos', $residuos);
    }

    /**
     * Show the form for creating a new Residuo.
     *
     * @return Response
     */
    public function create()
    {
        return view('residuos.create');
    }

    /**
     * Store a newly created Residuo in storage.
     *
     * @param CreateResiduoRequest $request
     *
     * @return Response
     */
    public function store(CreateResiduoRequest $request)
    {
        $input = $request->all();

        $residuo = $this->residuoRepository->create($input);

        Flash::success('Residuo guardado exitosamente.');

        return redirect(route('residuos.index'));
    }

    /**
     * Display the specified Residuo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $residuo = $this->residuoRepository->findWithoutFail($id);

        if (empty($residuo)) {
            Flash::error('Residuo not found');

            return redirect(route('residuos.index'));
        }

        return view('residuos.show')->with('residuo', $residuo);
    }

    /**
     * Show the form for editing the specified Residuo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $residuo = $this->residuoRepository->findWithoutFail($id);

        if (empty($residuo)) {
            Flash::error('Residuo not found');

            return redirect(route('residuos.index'));
        }

        return view('residuos.edit')->with('residuo', $residuo);
    }

    /**
     * Update the specified Residuo in storage.
     *
     * @param  int              $id
     * @param UpdateResiduoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResiduoRequest $request)
    {
        $residuo = $this->residuoRepository->findWithoutFail($id);

        

        if (empty($residuo)) {
            Flash::error('Residuo no encontrado');

            return redirect(route('residuos.index'));
        }

        $residuo = $this->residuoRepository->update($request->all(), $id);

        Flash::success('Residuo modificado exitosamente.');

        return redirect(route('residuos.index'));
    }

    /**
     * Remove the specified Residuo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('residuos.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Residuo::destroy($id);

           // Redirect to the group management page
           return redirect(route('residuos.index'))->with('success', Lang::get('message.success.delete'));

       }

}
