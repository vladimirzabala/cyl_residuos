<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use stdClass;
use Mail;
use App\Mail\Contact;


class EmailController extends Controller
{
    public function index(Request $request)
    {

       $menus =   collect(\DB::select("select * from menus where activo = 1 and menupadre = 0 and sitio ='PAGINA PRINCIPAL' order by orden"));
       $submenus =   collect(\DB::select("select * from menus where activo = 1 and menupadre <> 0 and sitio ='PAGINA PRINCIPAL' order by orden"));
	   return  view('contactenos',compact('menus', $menus),['submenus' => $submenus]);


	}





	public function postContact(Request $request)
    {

       try {


        	
     $data = new stdClass();

        // Data to be used on the email view
        $data->contact_name = $request->get('contact-name');
        $data->contact_email = $request->get('contact-email');
        $data->contact_msg = $request->get('contact-msg');

        // Send the activation code through email
        Mail::to('pruebas@gmail.com')->send(new Contact($data));

  

     
        return redirect('contactenos')->with('success', 'Su mensaje fue enviado correctamente.');


        } catch (Exception $e) {
        	   return redirect('contactenos')->with('error', 'Ha ocurrido un error al enviar su mensaje.');
        }
 



    }
	}
