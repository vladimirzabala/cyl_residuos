<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRecoleccion_residuoRequest;
use App\Http\Requests\UpdateRecoleccion_residuoRequest;
use App\Repositories\Recoleccion_residuoRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Recoleccion_residuo;
 use App\Models\Residuo;
 use App\Models\Proyecto;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class Recoleccion_residuoController extends InfyOmBaseController
{
    /** @var  Recoleccion_residuoRepository */
    private $recoleccionResiduoRepository;

    public function __construct(Recoleccion_residuoRepository $recoleccionResiduoRepo)
    {
        $this->recoleccionResiduoRepository = $recoleccionResiduoRepo;
    }

    /**
     * Display a listing of the Recoleccion_residuo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->recoleccionResiduoRepository->pushCriteria(new RequestCriteria($request));
        $recoleccionResiduos = $this->recoleccionResiduoRepository->all();
        return view('recoleccionResiduos.index')
            ->with('recoleccionResiduos', $recoleccionResiduos);
    }

    /**
     * Show the form for creating a new Recoleccion_residuo.
     *
     * @return Response
     */
    public function create()
    {

       
        $residuos = Residuo::pluck('nombre', 'id');

        return view('recoleccionResiduos.create', compact('residuos', $residuos));
       
    }

    /**
     * Store a newly created Recoleccion_residuo in storage.
     *
     * @param CreateRecoleccion_residuoRequest $request
     *
     * @return Response
     */
    public function store(CreateRecoleccion_residuoRequest $request)
    {


   $validarproyecto = Proyecto::where('id','=',$request->get('idproyecto'))->first();



   if (!$validarproyecto) { // INICIO VALIDACIÓN EXISTENCIA PROYECTO


     Flash::error('Número de proyecto no encontrado');
     return redirect(route('recoleccionResiduos.create'));

        
        }else
        {

        $input = $request->all();

        $recoleccionResiduo = $this->recoleccionResiduoRepository->create($input);

        Flash::success('La recolección ha sido almacenada exitosamente.');

        return redirect(route('recoleccionResiduos.index'));

        }// FIN VALIDACIÓN EXISTENCIA PROYECTO
    }

    /**
     * Display the specified Recoleccion_residuo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recoleccionResiduo = $this->recoleccionResiduoRepository->findWithoutFail($id);

        if (empty($recoleccionResiduo)) {
            Flash::error('Recoleccion de residuo no encontrada');

            return redirect(route('recoleccionResiduos.index'));
        }

        return view('recoleccionResiduos.show')->with('recoleccionResiduo', $recoleccionResiduo);
    }

    /**
     * Show the form for editing the specified Recoleccion_residuo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $recoleccionResiduo = $this->recoleccionResiduoRepository->findWithoutFail($id);

        if (empty($recoleccionResiduo)) {
            Flash::error('Recolección de residuo no encontrada');

            return redirect(route('recoleccionResiduos.index'));
        }

        $residuos = Residuo::pluck('nombre', 'id');

        return view('recoleccionResiduos.edit', compact('recoleccionResiduo', $recoleccionResiduo),['residuos' => $residuos]);

       
    }

    /**
     * Update the specified Recoleccion_residuo in storage.
     *
     * @param  int              $id
     * @param UpdateRecoleccion_residuoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecoleccion_residuoRequest $request)
    {

   $validarproyecto = Proyecto::where('id','=',$request->get('idproyecto'))->first();



   if (!$validarproyecto) { // INICIO VALIDACIÓN EXISTENCIA PROYECTO


     Flash::error('Número de proyecto no encontrado');
     return redirect(route('recoleccionResiduos.create'));

        
        }else
        {

            $recoleccionResiduo = $this->recoleccionResiduoRepository->findWithoutFail($id);

            

            if (empty($recoleccionResiduo)) {
                Flash::error('Recoleccion_residuo not found');

                return redirect(route('recoleccionResiduos.index'));
            }

            $recoleccionResiduo = $this->recoleccionResiduoRepository->update($request->all(), $id);

            Flash::success('Recolección de residuo modificada exitosamente.');

            return redirect(route('recoleccionResiduos.edit'));
        } // FIN  VALIDACIÓN EXISTENCIA PROYECTO



    }

    /**
     * Remove the specified Recoleccion_residuo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('recoleccionResiduos.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Recoleccion_residuo::destroy($id);

           // Redirect to the group management page
           return redirect(route('recoleccionResiduos.index'))->with('success', Lang::get('message.success.delete'));

       }

}
