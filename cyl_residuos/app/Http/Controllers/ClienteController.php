<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClienteRequest;
use App\Http\Requests\UpdateClienteRequest;
use App\Repositories\ClienteRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Cliente;
use App\Models\Empresa;
use App\User;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ClienteController extends InfyOmBaseController
{
    /** @var  ClienteRepository */
    private $clienteRepository;

    public function __construct(ClienteRepository $clienteRepo)
    {
        $this->clienteRepository = $clienteRepo;
    }

    /**
     * Display a listing of the Cliente.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->clienteRepository->pushCriteria(new RequestCriteria($request));

        $clientes = collect(\DB::select("select cli.id, e.id as IdEmpresa, us.id as IdUsuario, e.nombre, us.first_name,us.last_name
from  users us inner join clientes cli on cli.IdUsuario = us.id  inner join empresas e on e.id = cli.IdEmpresa 
 "));
        return view('admin.clientes.index')
            ->with('clientes', $clientes);
    }

    /**
     * Show the form for creating a new Cliente.
     *
     * @return Response
     */
    public function create()
    {
        $empresas = Empresa::pluck('nombre', 'id');
       // $usuarios = User::pluck('first_name'+'last_name','id');
        $usuarios = User::select(
            \DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')
            ->pluck('name', 'id');
        
        return view('admin.clientes.create', compact('empresas', $empresas),['usuarios'=>$usuarios]);
    }

    /**
     * Store a newly created Cliente in storage.
     *
     * @param CreateClienteRequest $request
     *
     * @return Response
     */
    public function store(CreateClienteRequest $request)
    {


         //verificar si ya se ha asignado un usuarios a una empresa
     $Cliente = Cliente::where('IdEmpresa','=',$request->get('idEmpresa'))->where('IdUsuario',$request->get('idUsuario'))->first();



      if (!$Cliente) { // INICIO VALIDACIÓN RELACION USUARIO EMPRESA



                    $input = $request->all();

                    $cliente = $this->clienteRepository->create($input);

                    Flash::success('Cliente asignado exitosamente.');

                    return redirect(route('admin.clientes.index'));


          }
        else
        {
        Flash::error('El cliente ya ha sido asignado a una empresa');
           return redirect(route('admin.clientes.index'));       



         }
    }

    /**
     * Display the specified Cliente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cliente = $this->clienteRepository->findWithoutFail($id);

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        return view('admin.clientes.show')->with('cliente', $cliente);
    }

    /**
     * Show the form for editing the specified Cliente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {/*
        $cliente = $this->clienteRepository->findWithoutFail($id);

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        return view('admin.clientes.edit')->with('cliente', $cliente);
        */
    }

    /**
     * Update the specified Cliente in storage.
     *
     * @param  int              $id
     * @param UpdateClienteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClienteRequest $request)
    {/*
        $cliente = $this->clienteRepository->findWithoutFail($id);

        

        if (empty($cliente)) {
            Flash::error('Cliente not found');

            return redirect(route('clientes.index'));
        }

        $cliente = $this->clienteRepository->update($request->all(), $id);

        Flash::success('Cliente updated successfully.');

        return redirect(route('admin.clientes.index'));
        */
    }

    /**
     * Remove the specified Cliente from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.clientes.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Cliente::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.clientes.index'))->with('success', Lang::get('message.success.delete'));

       }

}
