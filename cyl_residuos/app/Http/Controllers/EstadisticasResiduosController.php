<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Flash;
use stdClass;
use Mail;
use Charts;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Cliente;
use App\Models\Proyecto;
use App\Models\Usuarios_proyecto;
use App\Models\Archivo_certificados;
use Sentinel;
use App\Models\Empresa;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;

class EstadisticasResiduosController extends InfyOmBaseController
{
    

   public function indexestadisticas()
    {

   $IdUsuario = Sentinel::getUser()->id;
   

  $proyectosautorizados = Proyecto::join('usuarios_proyectos', 'proyectos.id', '=', 'usuarios_proyectos.id_proyecto')
          ->where('usuarios_proyectos.id_usuario', $IdUsuario)
          ->pluck('proyectos.nombre','proyectos.id')
          ->all();


  //collect(\DB::select("select p.nombre from proyectos p inner join usuarios_proyectos up on up.id_proyecto = p.id where up.id_usuario =".$IdUsuario));


        return view('recoleccionResiduos.index_estadisticas')
            ->with('proyectosautorizados', $proyectosautorizados);

 
    }

    



    

    public function estadisticasresiduos(Request $request)
    { // INICIO METODO

       
        $IdUsuario = Sentinel::getUser()->id;
        $IdEmpresa = Cliente::where('IdUsuario','=',$IdUsuario)->first();



   if (!$IdEmpresa) { // INICIO VALIDACIÓN RELACION USUARIO EMPRESA



     Flash::error('Usted no esta relacionado con ninguna empresa por favor pongase en contacto con el administrador');
     return redirect(route('my-account'));



        
        }else
        {

                  // Validar vinculación con el proyecto

           $validarvinculacionproyecto = Usuarios_proyecto::where('id_usuario','=',$IdUsuario)->where('id_proyecto',$request->get('id_proyecto'))->first();
         if (!$validarvinculacionproyecto ) { // INICIO VALIDACIÓN DE VINCULACIÓN PROYECTO



             Flash::error('Usted no esta relacionado con el proyecto');
             return redirect(route('my-account'));



                
                }else
                {


                    $IdUsuario = Sentinel::getUser()->id;


                    $proyectosautorizados = Proyecto::join('usuarios_proyectos', 'proyectos.id', '=', 'usuarios_proyectos.id_proyecto')
                    ->where('usuarios_proyectos.id_usuario', $IdUsuario)
                    ->pluck('proyectos.nombre','proyectos.id')
                    ->all();




                    $residuos = collect(\DB::select("select r.nombre,recol.cantidad,recol.unidad,recol.fecha
                    from recoleccion_residuos recol inner join residuos r on recol.residuo = r.id  where recol.idproyecto=".$request->get('id_proyecto')."  and year(recol.fecha)=".$request->get('año')."  and month(recol.fecha)=".$request->get('mes')." order by fecha"));





                    $label_grafico ="";
                    $cantidad_grafico =  "";
                    $tabla_grafico = $residuos;



                    foreach($residuos as $residuo){

                    $cantidad_grafico = $cantidad_grafico."'".$residuo->cantidad."'".",";
                    $label_grafico = $label_grafico."'".$residuo->fecha."-".$residuo->nombre."-".$residuo->unidad."'".",";  



                    }


                    $cantidades = rtrim($cantidad_grafico , ',');
                    $labels = rtrim($label_grafico, ',');





                    return view('recoleccionResiduos.estadisticas', compact('labels', 'cantidades','tabla_grafico'),['proyectosautorizados'=>$proyectosautorizados]);












           }// FIN  VALIDACIÓN DE VINCULACIÓ PROYECTO


        }// FIN  VALIDACIÓN RELACION USUARIO EMPRESA


  

      



    }// FIN METODO


    public function certificadoIndex()
    { 

   $IdUsuario = Sentinel::getUser()->id;
   

   $proyectosautorizados = Proyecto::join('usuarios_proyectos', 'proyectos.id', '=', 'usuarios_proyectos.id_proyecto')
          ->where('usuarios_proyectos.id_usuario', $IdUsuario)
          ->pluck('proyectos.nombre','proyectos.id')
          ->all();

   return view('recoleccionResiduos.index_certificado')
            ->with('proyectosautorizados', $proyectosautorizados);




    }


      public function reporteexcelIndex()
    { 

   $IdUsuario = Sentinel::getUser()->id;
   

   $proyectosautorizados = Proyecto::join('usuarios_proyectos', 'proyectos.id', '=', 'usuarios_proyectos.id_proyecto')
          ->where('usuarios_proyectos.id_usuario', $IdUsuario)
          ->pluck('proyectos.nombre','proyectos.id')
          ->all();

   return view('recoleccionResiduos.index_reporteexcel')
            ->with('proyectosautorizados', $proyectosautorizados);




    }


    public function reporteexcel(Request $request)
    { // INICIO METODO

       
        $IdUsuario = Sentinel::getUser()->id;
        $IdEmpresa = Cliente::where('IdUsuario','=',$IdUsuario)->first();



   if (!$IdEmpresa) { // INICIO VALIDACIÓN RELACION USUARIO EMPRESA



     Flash::error('Usted no esta relacionado con ninguna empresa por favor pongase en contacto con el administrador');
     return redirect(route('my-account'));



        
        }else
        {

                  // Validar vinculación con el proyecto

           $validarvinculacionproyecto = Usuarios_proyecto::where('id_usuario','=',$IdUsuario)->where('id_proyecto',$request->get('id_proyecto'))->first();
         if (!$validarvinculacionproyecto ) { // INICIO VALIDACIÓN DE VINCULACIÓN PROYECTO



             Flash::error('Usted no esta relacionado con el proyecto');
             return redirect(route('my-account'));



                
                }else
                {


                    $IdUsuario = Sentinel::getUser()->id;



                 




                    $residuos = collect(\DB::select("select r.nombre,recol.cantidad,recol.unidad,recol.fecha
                    from recoleccion_residuos recol inner join residuos r on recol.residuo = r.id  where recol.idproyecto=".$request->get('id_proyecto')."  and year(recol.fecha)=".$request->get('año')."  and month(recol.fecha)=".$request->get('mes')." order by fecha"));

                    $nombreArchivo ='residuos-'.$request->get('año').'-'.$request->get('mes');





                
                   //  return Excel::download($residuos, 'residuos-'.$request->get('año').'-'.$request->get('mes').'.xlsx');

                     return Excel::download(new class($residuos) implements FromCollection{

            public function __construct($collection)
            {
                $this->collection = $collection;
            }
            public function collection()
            {
                return $this->collection;
            }
},$nombreArchivo.".xlsx");




//dd($residuos);














           }// FIN  VALIDACIÓN DE VINCULACIÓ PROYECTO


        }// FIN  VALIDACIÓN RELACION USUARIO EMPRESA


  

      



    }// FIN METODO





        public function certificadopdf(Request $request) 
    {
        




        $IdUsuario = Sentinel::getUser()->id;
        $IdEmpresa = Cliente::where('IdUsuario','=',$IdUsuario)->first();



      if (!$IdEmpresa) { // INICIO VALIDACIÓN RELACION USUARIO EMPRESA



           Flash::error('Usted no esta relacionado con ninguna empresa por favor pongase en contacto con el administrador');
           return redirect(route('my-account'));



        
        }
        else
        {

        
    

        $residuos = collect(\DB::select("select r.id,r.nombre,rr.fecha,SUM(rr.cantidad)as cantidad,rr.unidad,r.tratamiento FROM recoleccion_residuos rr inner join residuos r on rr.residuo = r.id where rr.idproyecto =".$request->get('id_proyecto')."  and year(rr.fecha)=".$request->get('año')."  and month(rr.fecha)=".$request->get('mes')." group by r.id order by fecha"));




     

        $informacion_proyecto = collect(\DB::select("select empresas.nombre as nombre_empresa,proyectos.puntorecoleccion,proyectos.ciudad,proyectos.generador,proyectos.direccion from proyectos inner join empresas on proyectos.idempresa = empresas.id where proyectos.id =".$request->get('id_proyecto')));


          $info_archivo = "";
          $info_residuos = "";
          $save_archivo_proyecto = $informacion_proyecto;
          $save_residuos =  $residuos;

          foreach($save_archivo_proyecto as $save_archivo){

          $info_archivo = "Empresa: ".$save_archivo->nombre_empresa."<br>"."Punto de recolección: ".$save_archivo->puntorecoleccion."<br>"."Ciudad: ".$save_archivo->ciudad."<br>"."Generador: ".$save_archivo->generador."<br>"."Dirección: ".$save_archivo->direccion.".<br>";              

          }


  foreach($save_residuos as $residuo){

         $info_residuos = $residuo->id.",".$residuo->nombre.",".$residuo->fecha.",".$residuo->cantidad.",".$residuo->unidad.".<br>";          

          }
                 $id_usuario = Sentinel::getUser()->id;

                 $now = new \DateTime();
               

                 $archivos_usuarios = Archivo_certificados::create([
                  'datos_proyecto' => $info_archivo,
                  'residuos' => $info_residuos,
                  'id_usuario' => $id_usuario,                
                  ]);
                  $no_certificado=$archivos_usuarios->id;                   
                  $fecha_certificado= $archivos_usuarios->created_at->format('Y'); 
                  $fecha_expedicion= $archivos_usuarios->created_at->format('d/m/Y'); 

                  // encriptar codigo 

                  $codigo = \Hash::make("usuario-".$id_usuario."-fecha-".$now->format('d/m/Y-H:i:s')."-".$no_certificado);

                //  modificar codigo en la tabla archivo_certificados
                  \DB::table('archivo_certificados')->where('id', $no_certificado)->update(['codigo' => $codigo]);

                 
                

          // return view('pdf.certificado', compact('residuos','informacion_proyecto','no_certificado','fecha_certificado','codigo','fecha_expedicion'));

        $pdf = PDF::loadView('pdf.certificado', compact('residuos','informacion_proyecto','no_certificado','fecha_certificado','codigo','fecha_expedicion'));


        return $pdf->download('certificado.pdf');

/*
        $view =  \View::make('pdf.certificado', compact('collections'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView($view);
        return $pdf->stream('invoice');
         //return $pdf->download('Certificado_20.pdf');
         */

      }

       
    }

    public function getData() 
    {
        $data =  [
            'quantity'      => '1' ,
            'description'   => 'some ramdom text',
            'price'   => '500',
            'total'     => '500'
        ];
        return $data;
    }




    

}
