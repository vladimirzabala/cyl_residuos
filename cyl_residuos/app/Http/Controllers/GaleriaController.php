<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateGaleriaRequest;
use App\Http\Requests\UpdateGaleriaRequest;
use App\Repositories\GaleriaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Galeria;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GaleriaController extends InfyOmBaseController
{
    /** @var  GaleriaRepository */
    private $galeriaRepository;

    public function __construct(GaleriaRepository $galeriaRepo)
    {
        $this->galeriaRepository = $galeriaRepo;
    }

    /**
     * Display a listing of the Galeria.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->galeriaRepository->pushCriteria(new RequestCriteria($request));
        $galerias = $this->galeriaRepository->all();
        return view('galeria.index')
            ->with('galerias', $galerias);
    }

    /**
     * Show the form for creating a new Galeria.
     *
     * @return Response
     */
    public function create()
    {
        return view('galeria.create');
    }

    /**
     * Store a newly created Galeria in storage.
     *
     * @param CreateGaleriaRequest $request
     *
     * @return Response
     */
    public function store(CreateGaleriaRequest $request)
    {
        $input = $request->all();

        $galeria = $this->galeriaRepository->create($input);

        Flash::success('Galeria saved successfully.');

        return redirect(route('galeria.index'));
    }

    /**
     * Display the specified Galeria.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $galeria = $this->galeriaRepository->findWithoutFail($id);

        if (empty($galeria)) {
            Flash::error('Galeria not found');

            return redirect(route('galeria.index'));
        }

        return view('galeria.show')->with('galeria', $galeria);
    }

    /**
     * Show the form for editing the specified Galeria.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $galeria = $this->galeriaRepository->findWithoutFail($id);

        if (empty($galeria)) {
            Flash::error('Galeria not found');

            return redirect(route('galeria.index'));
        }

        return view('galeria.edit')->with('galeria', $galeria);
    }

    /**
     * Update the specified Galeria in storage.
     *
     * @param  int              $id
     * @param UpdateGaleriaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGaleriaRequest $request)
    {
        $galeria = $this->galeriaRepository->findWithoutFail($id);

        

        if (empty($galeria)) {
            Flash::error('Galeria not found');

            return redirect(route('galeria.index'));
        }

        $galeria = $this->galeriaRepository->update($request->all(), $id);

        Flash::success('Galeria updated successfully.');

        return redirect(route('galeria.index'));
    }

    /**
     * Remove the specified Galeria from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('galeria.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Galeria::destroy($id);

           // Redirect to the group management page
           return redirect(route('galeria.index'))->with('success', Lang::get('message.success.delete'));

       }

}
