<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUsuarios_proyectoRequest;
use App\Http\Requests\UpdateUsuarios_proyectoRequest;
use App\Repositories\Usuarios_proyectoRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Usuarios_proyecto;
use App\Models\Proyecto;
use App\User;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class Usuarios_proyectoController extends InfyOmBaseController
{
    /** @var  Usuarios_proyectoRepository */
    private $usuariosProyectoRepository;

    public function __construct(Usuarios_proyectoRepository $usuariosProyectoRepo)
    {
        $this->usuariosProyectoRepository = $usuariosProyectoRepo;
    }

    /**
     * Display a listing of the Usuarios_proyecto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->usuariosProyectoRepository->pushCriteria(new RequestCriteria($request));


          $usuariosProyectos  = collect(\DB::select("select up.id,p.nombre as nombreproyecto,u.first_name,u.last_name from proyectos p inner join usuarios_proyectos up on up.id_proyecto = p.id inner join users u on u.id = up.id_usuario"));
         return view('admin.usuariosProyectos.index')
            ->with('usuariosProyectos', $usuariosProyectos);
    }

    /**
     * Show the form for creating a new Usuarios_proyecto.
     *
     * @return Response
     */
    public function create()
    {




         $proyectos = Proyecto::pluck('nombre', 'id');
       // $usuarios = User::pluck('first_name'+'last_name','id');
        $usuarios = User::select(
            \DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')
            ->pluck('name', 'id');
        
        return view('admin.usuariosProyectos.create', compact('proyectos', $proyectos),['usuarios'=>$usuarios]);
    }

    /**
     * Store a newly created Usuarios_proyecto in storage.
     *
     * @param CreateUsuarios_proyectoRequest $request
     *
     * @return Response
     */
    public function store(CreateUsuarios_proyectoRequest $request)
    {
        
        //verificar si ya se ha asignado un usuario al proyecto
         $UsuariosProyecto = Usuarios_proyecto::where('id_usuario','=',$request->get('Id_usuario'))->where('id_proyecto',$request->get('id_proyecto'))->first();



      if (!$UsuariosProyecto) { // INICIO VALIDACIÓN RELACION USUARIO PROYECTO


        $input = $request->all();

        $usuariosProyecto = $this->usuariosProyectoRepository->create($input);

        Flash::success('Se ha asignado el proyecto exitosamente.');

        return redirect(route('admin.usuariosProyectos.index'));          



        
        }
        else
        {
        Flash::error('El usuario ya ha sido relacionado con el proyecto');
           return redirect(route('admin.usuariosProyectos.index'));       



         }




    }

    /**
     * Display the specified Usuarios_proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
   /* public function show($id)
    {
        $usuariosProyecto = $this->usuariosProyectoRepository->findWithoutFail($id);

        if (empty($usuariosProyecto)) {
            Flash::error('Usuarios_proyecto not found');

            return redirect(route('usuariosProyectos.index'));
        }

        return view('admin.usuariosProyectos.show')->with('usuariosProyecto', $usuariosProyecto);
    }*/

    /**
     * Show the form for editing the specified Usuarios_proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
   /* public function edit($id)
    {
        $usuariosProyecto = $this->usuariosProyectoRepository->findWithoutFail($id);

        if (empty($usuariosProyecto)) {
            Flash::error('Usuarios_proyecto not found');

            return redirect(route('usuariosProyectos.index'));
        }

        return view('admin.usuariosProyectos.edit')->with('usuariosProyecto', $usuariosProyecto);
    }*/

    /**
     * Update the specified Usuarios_proyecto in storage.
     *
     * @param  int              $id
     * @param UpdateUsuarios_proyectoRequest $request
     *
     * @return Response
     */
 /*   public function update($id, UpdateUsuarios_proyectoRequest $request)
    {
        $usuariosProyecto = $this->usuariosProyectoRepository->findWithoutFail($id);

        

        if (empty($usuariosProyecto)) {
            Flash::error('Usuarios_proyecto not found');

            return redirect(route('usuariosProyectos.index'));
        }

        $usuariosProyecto = $this->usuariosProyectoRepository->update($request->all(), $id);

        Flash::success('Usuarios_proyecto updated successfully.');

        return redirect(route('admin.usuariosProyectos.index'));
    }*/

    /**
     * Remove the specified Usuarios_proyecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.usuariosProyectos.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Usuarios_proyecto::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.usuariosProyectos.index'))->with('success', Lang::get('message.success.delete'));

       }

}
