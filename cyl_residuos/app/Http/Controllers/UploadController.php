<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUploadRequest;
use App\Http\Requests\UpdateUploadRequest;
use App\Repositories\UploadRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Upload;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;

class UploadController extends InfyOmBaseController
{
    /** @var  UploadRepository */
    private $uploadRepository;

    public function __construct(UploadRepository $uploadRepo)
    {
        $this->uploadRepository = $uploadRepo;
    }

    /**
     * Display a listing of the Upload.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {


                if(Sentinel::inRole('laboratorio'))
                {
                   $uploads = $this->uploadRepository->indexUploadLaboratorio();
                   return view('uploads.start')->with('uploads', $uploads);
                }else
                {
                        $this->uploadRepository->pushCriteria(new RequestCriteria($request));
                        $uploads = $this->uploadRepository->all();
                      
                        return view('uploads.start')->with('uploads', $uploads);
                }

    }

    /**
     * Show the form for creating a new Upload.
     *
     * @return Response
     */
    public function create()
    {
        return view('uploads.crear');
    }

    /**
     * Store a newly created Upload in storage.
     *
     * @param CreateUploadRequest $request
     *
     * @return Response
     */
    public function store(CreateUploadRequest $request)
    {
      

try{
    if(!\Request::file("file"))
    {
        Flash::error('Por favor elija un archivo.');
        return view('uploads.crear')->with('error-message', 'Por favor elija un archivo');
    }
 
    $mime = \Request::file('file')->getMimeType();
    $extension = strtolower(\Request::file('file')->getClientOriginalExtension());
    $fileName = uniqid().'.'.$extension;
    $path = public_path() . '/uploads/files/';
 
    switch ($mime)
    {
       case "image/jpeg":
       case "image/png":
       case "image/gif":
       case "application/pdf":
       case "application/zip":
       case "application/octet-stream":
       case "audio/mpeg":
       case "application/vnd.ms-excel":
       case "application/vnd.ms-excel.sheet.binary.macroEnabled.12":
       case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
       case "application/msword":
       case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
       case "image/tiff":
       case "application/vnd.ms-excel":
       case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
            if (\Request::file('file')->isValid())
            {
                \Request::file('file')->move($path, $fileName);
                $upload = new Upload();
                $upload->filename = $fileName;
                $upload->tipo=  $request->tipo;
                $upload->descripcion=  $request->descripcion;
               
                     $upload->sitio =  'RESIDUOS';

               

               
                if($upload->save())
                {

                     Flash::success('El archivo ha sido almacenado');

                                                        //METODO INDEX
                                                           if(Sentinel::inRole('laboratorio'))
                                                            {
                                                               $uploads = $this->uploadRepository->indexUploadLaboratorio();
                                                               return view('uploads.start')->with('uploads', $uploads);
                                                            }else
                                                            {
                                                                    $this->uploadRepository->pushCriteria(new RequestCriteria($request));
                                                                    $uploads = $this->uploadRepository->all();
                                                                  
                                                                    return view('uploads.start')->with('uploads', $uploads);
                                                            }
               
                }
                else
                {
                    \File::delete($path."/".$fileName);
                  
                 Flash::error('Se produjo un error al guardar los datos en la base de datos');

            return view('uploads.crear');
                }
            }
        break;
        default:
          Flash::error('El tipo  de extensión del archivo no es válido.');
            return view('uploads.crear')->with('error-message', 'El tipo  de extensión del archivo no es válido');
    
    }
   } catch (GroupNotFoundException $e) {

                  Flash::error('Ha ocurrido un error por favor contacte al administrador.');

                    //METODO INDEX
                  if(Sentinel::inRole('laboratorio'))
                        {
                           $uploads = $this->uploadRepository->indexUploadLaboratorio();
                           return view('uploads.start')->with('uploads', $uploads);
                        }else
                        {
                                $this->uploadRepository->pushCriteria(new RequestCriteria($request));
                                $uploads = $this->uploadRepository->all();
                              
                                return view('uploads.start')->with('uploads', $uploads);
                        }
           
   }
    


}

    /**
     * Display the specified Upload.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $upload = $this->uploadRepository->findWithoutFail($id);

        if (empty($upload)) {
            Flash::error('Upload not found');

           $uploads = $this->uploadRepository->all();
        return view('uploads.start')
            ->with('uploads', $uploads);
        }

        return view('uploads.show')->with('upload', $upload);
    }

    /**
     * Show the form for editing the specified Upload.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $upload = $this->uploadRepository->findWithoutFail($id);

        if (empty($upload)) {
            Flash::error('Upload not found');

            $uploads = $this->uploadRepository->all();
        return view('uploads.start')
            ->with('uploads', $uploads);
        }

        return view('uploads.edit')->with('upload', $upload);
    }

    /**
     * Update the specified Upload in storage.
     *
     * @param  int              $id
     * @param UpdateUploadRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUploadRequest $request)
    {
        $upload = $this->uploadRepository->findWithoutFail($id);

        

        if (empty($upload)) {
            Flash::error('Upload not found');

           $uploads = $this->uploadRepository->all();
        return view('uploads.start')
            ->with('uploads', $uploads);
        }

        $upload = $this->uploadRepository->update($request->all(), $id);

        Flash::success('El archivo ha sido actualizado.');
       $uploads=Upload::orderBy('id', 'DESC')->paginate();
        return view('uploads.start',compact('$uploads', $uploads));
    }

    /**
     * Remove the specified Upload from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('uploads.delete',['id'=>$id]);
          return View('layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete(Request $request,$id = null)
       {
        try{


        $path = public_path() . '/uploads/files/';   
        $upload = Upload::where('id', '=', $id)->get()->first();           
        \File::delete($path."/".$upload->filename);
        $sample = Upload::destroy($id);
         // Redirect to the group management page
        Flash::success('El archivo ha sido eliminado.');
         $this->uploadRepository->pushCriteria(new RequestCriteria($request));
        $uploads = $this->uploadRepository->all();
      
        return view('uploads.start')->with('uploads', $uploads);
      
           
      
             
           } catch (GroupNotFoundException $e) {
             Flash::error('Ha ocurrido un error por favor contacte al administrador.');
                $this->uploadRepository->pushCriteria(new RequestCriteria($request));
        $uploads = $this->uploadRepository->all();
      
        return view('uploads.start')->with('uploads', $uploads);
           }
       }

}
