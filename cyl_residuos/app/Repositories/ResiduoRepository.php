<?php

namespace App\Repositories;

use App\Models\Residuo;
use InfyOm\Generator\Common\BaseRepository;

class ResiduoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'tratamiento'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Residuo::class;
    }
}
