<?php

namespace App\Repositories;

use App\Models\Upload;
use InfyOm\Generator\Common\BaseRepository;

class UploadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filename',
        'tipo',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Upload::class;
    }


    public function indexUploadLaboratorio()
    {
        return Upload::orderBy('id', 'DESC')->where('sitio', 'LABORATORIO')->paginate(10);
    }
}
