<?php

namespace App\Repositories;

use App\Models\Galeria;
use InfyOm\Generator\Common\BaseRepository;

class GaleriaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'ruta',
        'peso'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Galeria::class;
    }
}
