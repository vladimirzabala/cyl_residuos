<?php

namespace App\Repositories;

use App\Models\MensajeUsuarios;
use InfyOm\Generator\Common\BaseRepository;

class MensajeUsuariosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'telefono',
        'ciudad',
        'Correo',
        'tipoObra',
        'Direccion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MensajeUsuarios::class;
    }
}
