<?php

namespace App\Repositories;

use App\Models\Recoleccion_residuo;
use InfyOm\Generator\Common\BaseRepository;

class Recoleccion_residuoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'idproyecto',
        'residuo',
        'cantidad',
        'unidad',
        'fecha',
        'puntorecoleccion',
        'ciudad',
        'generador',
        'direccion',
        'tratamiento'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Recoleccion_residuo::class;
    }
}
