<?php

namespace App\Repositories;

use App\Models\Usuarios_proyecto;
use InfyOm\Generator\Common\BaseRepository;

class Usuarios_proyectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'Id_usuario'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Usuarios_proyecto::class;
    }
}
