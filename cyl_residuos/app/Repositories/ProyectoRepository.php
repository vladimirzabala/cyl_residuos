<?php

namespace App\Repositories;

use App\Models\Proyecto;
use InfyOm\Generator\Common\BaseRepository;

class ProyectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'idempresa'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Proyecto::class;
    }
}
