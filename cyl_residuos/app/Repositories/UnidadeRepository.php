<?php

namespace App\Repositories;

use App\Models\Unidade;
use InfyOm\Generator\Common\BaseRepository;

class UnidadeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Nombre',
        'Descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Unidade::class;
    }
}
