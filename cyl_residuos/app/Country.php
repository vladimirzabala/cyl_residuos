<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Country extends Model
{
    use Eloquence;


    protected $table = 'paises';
    protected $guarded  = ['id'];
    protected $searchableColumns = ['pais'];
}
